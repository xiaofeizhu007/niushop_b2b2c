<?php

/***********************************多点公共版本******************************************/

/**
 * @param DateTime|int|string|null $date 可以是DateTime对象、时间戳、字符串形式的日期或者空值表示当前月份
 * @return array [[1,2,3,4,5], ...] 分别表示这月每一周都是几号到几号
 */
function getWeeksOfMonth($year, $month){
    $date = $year."-".$month;
    $date = new DateTime($date);


    // 当前日期是在一个月里面是第几天
    //  j: 月份中的第几天，没有前导零    1 到 31
    $dateDay = (int)$date->format('j');

    // 这个月1号是星期几
    // N: 1（表示星期一）到 7（表示星期天）
    $beginWeekDay = (int)$date->sub(new DateInterval("P" . ($dateDay - 1) . "D"))->format('N');

    // 这个月最后一天是几号
    // j    月份中的第几天，没有前导零    1 到 31
    $endMonthDay = (int)($date->add(new DateInterval('P1M'))->sub(new DateInterval("P1D"))->format('j'));



    $weeks = [];
    $indexOfWeek = 0;
    $weekDay = $beginWeekDay;

    for ($day = 1; $day <= $endMonthDay; $day++){
        if (!isset($weeks[$indexOfWeek])){
            $weeks[$indexOfWeek] = [];
        }

        $weeks[$indexOfWeek][] = date($year."-".$month."-".$day);

        $weekDay++;
        if ($weekDay > 7){
            $weekDay = $weekDay - 7;
            $indexOfWeek++;
        }else{
            if($day == $endMonthDay){
                $day_num = 1;
                while($weekDay <= 7){
                    $last_time = date($year."-".$month."-".$day);
                    $weeks[$indexOfWeek][] = date('Y-m-d',strtotime("$last_time+".$day_num."day"));
                    $day_num++;
                    $weekDay++;
                }
            }
        }
    }
    $month_week_array = array();
    // 只要一个星期里面的第一天和最后一天？
    foreach ($weeks as $k => $week) {
        if(count($week) < 7){
            unset($weeks[$k]);
        }else{
            $week_string = date($week[0])."～".date(end($week));
            $week_first_day = $week[0];
            $week_last_day = end($week);
            $month_week_array[] = array("week_remark"=>$week_string, "week_first_day" => $week_first_day, "week_last_day" => $week_last_day);

        }
    }
    return $month_week_array;
}
/**
 * 数字转化为周日期写法
 */
function numChangeWeek($num){
    $array = ["一", "二", "三", "四", "五", "六", "日"];
    $week_string = $array[$num-1];
    return $week_string;
}