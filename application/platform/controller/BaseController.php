<?php
/**
 * BaseController.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\platform\controller;

\think\Loader::addNamespace('data', 'data/');
use data\service\AdminUser as User;
use data\service\Shop;
use data\service\WebSite as WebSite;
use data\service\Config as ConfigService;
use think\Controller;
use think\Config;

class BaseController extends Controller
{

    protected $user = null;

    protected $website = null;

    protected $uid;

    protected $instance_id;

    protected $instance_name;

    protected $user_name;

    protected $user_headimg;

    protected $module = null;

    protected $controller = null;

    protected $action = null;

    protected $module_info = null;

    protected $rootid = null;

    protected $moduleid = null;

    /**
     * 当前版本的路径
     *
     * @var string
     */
    protected $style = null;

    public function __construct()
    {
        parent::__construct();

        $this->user = new User();
        $this->website = new WebSite();
        $this->init();
        $this->assign("pageshow", PAGESHOW);
        $this->assign("pagesize", PAGESIZE);
    }
    /**
     * 创建时间：2016-10-27
     * 功能说明：action基类 调用 加载头部数据的方法
     */
    public function init()
    {
        $this->uid = $this->user->getSessionUid();
        $is_system = $this->user->getSessionUserIsSystem();
        
        if (empty($this->uid)) {
            if (request()->isAjax()) {
                echo json_encode(AjaxReturn(NO_LOGIN));
                exit();
                }
                else{
                    $this->redirect(__URL(__URL__.'/'. PLATFORM_MODULE . "/login"));
                }
         
        }
        if (empty($is_system)) {
            $this->redirect(__URL(__URL__.'/'. PLATFORM_MODULE. "/login"));
        }
        $this->instance_id = $this->user->getSessionInstanceId();
        $this->instance_name = $this->user->getInstanceName();
        $this->module = \think\Request::instance()->module();
        $this->controller = \think\Request::instance()->controller();
        $this->action = \think\Request::instance()->action();
        
        // 判断是否是插件菜单
        if (strpos($this->action, 'menu_') !== false) {
            $action_array = explode('_', $this->action);
            session('controller', $action_array[1]);
            $addons = request()->get('addons', '');
            $method = request()->get('method','');
            $redirect = __URL(__URL__ . '/' . PLATFORM_MODULE . "/Menu/addonmenu", 'addons=' . $addons.'&method='.$method);
            $this->redirect($redirect);
        }
        
        $this->module_info = $this->website->getModuleIdByModule($this->controller, $this->action);
        // 过滤控制权限 为0
        if (empty($this->module_info)) {
            $this->moduleid = 0;
            $check_auth = 1;
        } elseif ($this->module_info["is_control_auth"] == 0) {
            $this->moduleid = $this->module_info['module_id'];
            $check_auth = 1;
        } else {
            $this->moduleid = $this->module_info['module_id'];
            $check_auth = $this->user->checkAuth($this->moduleid);
        }
        // 版本
        @include ROOT_PATH . 'version.php';
        $this->assign('niu_version', NIU_VERSION);
        $this->assign('niu_ver_date', NIU_VER_DATE); 
            

        $shop = new Shop();
        $ShopNavigationData = $shop->ShopNavigationList(1, 6, '', 'sort');
        if ($check_auth) {
            $this->style = 'platform/';
            $this->addUserLog();
            if (! request()->isAjax()) {
                $web_info = $this->website->getWebSiteInfo();
                $user_info = $this->user->getUserInfo();
                $root_array = $this->website->getModuleRootAndSecondMenu($this->moduleid);
                $this->rootid = $root_array[0];
                $this->second_menu_id = $root_array[1];
                $this->getNavigation();
                $root_module_info = $this->website->getSystemModuleInfo($this->rootid, 'module_name,url,controller');
                if($this->second_menu_id != 0){
                    $three_menu_list = $this->user->getchildModuleQuery($this->second_menu_id);
                }else{
                    $three_menu_list = '';
                }
                $this->assign('three_menu_list', $three_menu_list);
                $this->user_name = $user_info['user_name'];
                $this->assign("headid", $this->rootid);
                $this->assign("second_menu_id", $this->second_menu_id);
                $this->assign("moduleid", $this->moduleid);
                $this->assign("HeadCode", $this->user_name);
                $this->assign("title_name", $this->instance_name);
                $this->assign("username", $this->user_name);
                
                // 获取后台设定的默认图片
                $config = new ConfigService();
                $defaultImages = $config->getDefaultImages($this->instance_id);
                $this->assign("default_goods_img", $defaultImages["value"]["default_goods_img"]);//默认商品图片
                $this->assign("default_headimg", $defaultImages["value"]["default_headimg"]);//默认用户头像
                $this->assign('default_shop_img', $defaultImages["value"]["default_shop_img"]);//默认店铺图片
                
                
                // 暂时这样写，以后改
                $this->assign("frist_menu", $root_module_info);
                $this->assign("secend_menu", $this->module_info);
                $child_menu_list = array(
                    array(
                        'url' => $this->module_info['url'],
                        'menu_name' => $this->module_info['module_name'],
                        'active' => 1
                    )
                );
                $this->assign('child_menu_list', $child_menu_list);
                $this->assign('ShopNavigationData', $ShopNavigationData['data']);
                $this->assign('controller', $this->controller);
                $this->assign('action', $this->action);
            }
        } else {
            if (request()->isAjax()) {
                echo json_encode(AjaxReturn(NO_AITHORITY));
                exit();
            } else {
                $this->error("当前用户没有操作权限");
            }
        }

        //处理客户端时区
        $clientTimezone = \think\Session::get('clientTimezone');
        if(!empty($clientTimezone)){
            date_default_timezone_set($clientTimezone);
        }
    }

    /**
     * 添加操作日志（当前考虑所有操作），
     */
    private function addUserLog()
    {
        $get_data = '';
        if ($_GET) {
            $res = \think\Request::instance()->get();
            $get_data = json_encode($res);
        }
        if ($_POST) {
            $res = \think\Request::instance()->post();
            if (empty($get_data)) {
                $get_data = json_encode($res);
            } else {
                $get_data = $get_data . ',' . json_encode($res);
            }
        }
        // 建议，调试模式，用于
        // $res = $this->user->addUserLog($this->uid, 1, $this->controller, $this->action, \think\Request::instance()->ip(), $get_data);
    }

    /**
     * 获取导航
     */
    public function getNavigation()
    {
        $first_list = $this->user->getchildModuleQuery(0);
        $list = array();
        foreach ($first_list as $k => $v) {
            $submenu = $this->user->getchildModuleQuery($v['module_id']);
            $list[$k]['data'] = $v;
            $list[$k]['sub_menu'] = $submenu;
        }
        $this->assign("nav_list", $list);
    }
    public function getOperationTips($tips){
        $tips_array = array();
        if(!empty($tips)){
            $tips_array = explode("///", $tips);
        }
        $this->assign("tips", $tips_array);
    }
    
    
    
    /**
     * 获取操作提示是否显示
     *
     * @return mixed|boolean|void
     */
    public function getWarmPromptIsShow()
    {
        $is_show = cookie("warm_promt_is_show");
        if ($is_show == null) {
            $is_show = 'show';
        }
        return $is_show;
    }
    
    /**
     * 获取系统信息
     */
    public function getSystemConfig()
    {
        $system_config['os'] = php_uname(); // 服务器操作系统
        $system_config['server_software'] = $_SERVER['SERVER_SOFTWARE']; // 服务器环境
        $system_config['upload_max_filesize'] = @ini_get('file_uploads') ? ini_get('upload_max_filesize') : 'unknow'; // 文件上传限制
        $system_config['gd_version'] = gd_info()['GD Version']; // GD（图形处理）版本
        $system_config['max_execution_time'] = ini_get("max_execution_time") . "秒"; // 最大执行时间
        $system_config['port'] = $_SERVER['SERVER_PORT']; // 端口
        $system_config['dns'] = $_SERVER['HTTP_HOST']; // 服务器域名
        $system_config['php_version'] = PHP_VERSION; // php版本
        $system_config['ip'] = $_SERVER['SERVER_ADDR']; // 服务器ip
        $system_config['sockets'] = extension_loaded('sockets'); //是否支付sockets
        $system_config['openssl'] = extension_loaded('openssl'); //是否支付openssl
        $system_config['curl'] = function_exists('curl_init'); // 是否支持curl功能
    
        $this->assign("system_config", $system_config);
    }
    
    /**
     * 获取三级菜单
     * 创建时间：2017年8月25日 17:49:07 王永杰
     * 目前只有固定模板和自定义模板用
     */
    public function getThreeLevelModule()
    {
        $child_menu_list_old = $this->user->getchildModuleQuery($this->second_menu_id);
        $child_menu_list = [];
        foreach ($child_menu_list_old as $k => $v) {
            $active = 0;
            $param = request()->param();
            if (strpos(strtolower(request()->pathinfo()), strtolower($v['url']))) {
                $active = 1;
            } else
                if (! empty($param['addons']) && strpos(strtolower($v['url']), strtolower($param['addons'])) !== false) {
                    $active = 1;
                }
            $child_menu_list[] = array(
                'url' => $v['url'],
                'menu_name' => $v['module_name'],
                'active' => $active
            );
        }
    
        $this->assign('child_menu_list', $child_menu_list);
    }
    
    
    
}
