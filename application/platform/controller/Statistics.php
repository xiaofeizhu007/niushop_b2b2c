<?php
namespace app\platform\controller;

use data\service\Platform;
use data\service\Shop;
use data\service\Goods;
use data\service\GoodsCategory;
use data\service\Member;
use data\service\Statistics as StatisticsService;
use think\helper\Time;
use data\service\Order;

/**
 * 系统模块控制器
 *
 * @author Administrator
 *        
 */
class Statistics extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 统计 概述
     */
    public function statGeneral()
    {
        
        $statistics_service = new StatisticsService();
        
        if (request()->isAjax()) {
            $pageindex = isset($_POST['pageIndex']) ? $_POST['pageIndex'] : 1;
            $start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
            $end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2099-1-1';
            $condition["create_time"] = [
                [
                    ">",
                    $start_date
                ],
                [
                    "<",
                    $end_date
                ]
            ];
            $platform = new Platform();

            $count_list = $platform->getPlatformCount($start_date, $end_date);
            $list = $platform->getPlatformAccountRecordsList($pageindex, PAGESIZE, $condition, 'create_time desc');
            // $count_list = $platform->getPlatformCount("2017-03-12", "2017-04-12");
            $count = [
                "count_list" => $count_list,
                "page_lsit" => $list
            ];
            // $this->assign("count_list",$count_list);
            return $count;
        }
        else
        {
            $member_count = $statistics_service->getMemberCount(array()); //会员总数量

            $integral_sum = $statistics_service->getIntegralSum(array()); //总积分数
            $grant_integral_sum = $statistics_service->getGrantIntegralSum(array()); //发放积分总数

            $this->assign('member_count',$member_count);
            $this->assign('integral_sum',$integral_sum);
            $this->assign('grant_integral_sum',$grant_integral_sum);
            
            
            
            // 店铺销售排行
            $shop_rank = $this->getShopRealSalesRank();
            $this->assign("shop_rank", $shop_rank);
            // 商品销售排行
            $goods_rank = $this->getGoodsRealSalesRank();
            $this->assign("goods_list", $goods_rank);
            
            $this->assign('today',time());
            return view($this->style . 'Statistics/statGeneral');
        }
    }
    
    /**
     * 平台相关情报统计
     *
     */
    public function getCountInfo(){
        
        $statistics_service = new StatisticsService();
        
        $type = request()->post('type',1);
        $year = request()->post('year');
        $month = request()->post('month');
        $week = request()->post('week');
        $day = request()->post('day');
        
        if($type == 1){
            
            $start_time = getTimeTurnTimeStamp($day);
            $end_time = $start_time + 3600 * 24;
        }else if($type == 3){
            
            $start_time = getTimeTurnTimeStamp($week);
            $end_time = $start_time + 3600 * 24 * 7;
        }else if($type == 4){
            
            $month_start = $year."-".$month."-01";
            $month_end =  date("Y-m-d",strtotime("$month_start + 1 month"));
     
            $start_time = getTimeTurnTimeStamp($month_start);
            $end_time = getTimeTurnTimeStamp($month_end);
        }
        
        $condition['create_time'] = ['between',[$start_time, $end_time]];
        
        //下单金额
        $count_money = $this->getShopSaleData($condition, 1);
        
        //下单量
        $count_num = $this->getShopSaleData($condition, 2);
        
        //订单会员统计
        $order_member_count = $statistics_service->getOrderMemberCount($condition);
        
        //订单商品统计
        $order_goods_count = $statistics_service->getSaleGoodsCount($condition);
        
        //新增会员统计
        $add_member_count = $statistics_service->getMemberCount([
            'reg_time'=>
            [
                'between',[$start_time, $end_time]
            ]
        ]);
        
        //新增商品统计
        $add_goods_count = $statistics_service->getGoodsCount($condition);
        
        //新增店铺统计
        $add_shop_count = $statistics_service->getShopCount([
            'shop_create_time'=>
            [
                'between',[$start_time, $end_time]
            ]
        ]);
        
        $data['count_money'] = $count_money;
        $data['count_num'] = $count_num;
        $data['add_member_count'] = $add_member_count;
        $data['add_goods_count'] = $add_goods_count;
        $data['add_shop_count'] = $add_shop_count;
        $data['order_member_count'] = $order_member_count;
        $data['order_goods_count'] = $order_goods_count;
        
        return $data;
    }
    
    /**
     * 获取某一年某一月的所有周
     */
    public function getWeeks(){
        $year = request()->post('year');
        $month = request()->post('month');
        
        $res = getWeeksOfMonth($year, $month);
        
        return $res;
    }

    /**
     * 会员统计
     *
     * @return \think\response\View
     */
    public function userStat()
    {
        $user = new Member();
        $user_count_num = $user->getMemberCount(array());
        //php获取今日开始时间戳和结束时间戳
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        $condition_one["reg_time"] = [
            [
                ">",
                $beginToday
            ],
            [
                "<",
                $endToday
            ]
        ];
        $user_today_num = $user->getMemberCount($condition_one);

        //php获取本月起始时间戳和结束时间戳
        $beginThismonth = mktime(0,0,0,date('m'),1,date('Y'));
        $endThismonth = mktime(23,59,59,date('m'),date('t'),date('Y'));
        $condition["reg_time"] = [
            [
                ">",
                $beginThismonth
            ],
            [
                "<",
                $endThismonth
            ]
        ];
        $user_month_num = $user->getMemberCount($condition);
        
        $month_begin = date('Y-m-01', strtotime(date("Y-m-d")));
        $month_end = date('Y-m-d', strtotime("$month_begin +1 month -1 day"));
        
        $this->assign("user_count_num", $user_count_num);
        $this->assign("user_today_num", $user_today_num);
        $this->assign("user_month_num", $user_month_num);
        $this->assign("start_date", $month_begin);
        $this->assign("end_date", $month_end);
        return view($this->style . 'Statistics/userStat');
    }

    /**
     * 店铺统计
     *
     * @return \think\response\View
     */
    public function shopStat()
    {
        $shop = new Shop();
        $condition = array();
        $shop_list = $shop->getShopAll($condition);
        $this->assign("shop_list", $shop_list);
        return view($this->style . 'Statistics/shopStat');
    }

    /**
     * 订单统计
     *
     * @return \think\response\View
     */
    public function orderStat()
    {
        if(request()->isAjax())
        {
            $pageindex = isset($_POST['pageIndex']) ? $_POST['pageIndex'] : 1;
            $start_date = request()->post('start_date');
            $end_date = request()->post('end_date');
            $search_text = request()->post('search_text');
            $shop_id = request()->post('shop_id');
            $condition = array();
            
            if(!empty($search_text)){
                $condition['order_no'] = $search_text;
            }
            
            if(!empty($shop_id) || $shop_id == '0'){
                $condition['shop_id'] = $shop_id;
            }
            
            if($start_date != '' && $end_date != ''){
                $condition["create_time"] = [
                    [
                        ">",
                        strtotime($start_date)
                    ],
                    [
                        "<",
                        strtotime($end_date)
                    ]
                ];
                $count_condition["create_time"] = [
                    [
                        ">",
                        strtotime($start_date)
                    ],
                    [
                        "<",
                        strtotime($end_date)
                    ]
                ];
            }else if($start_date != '' && $end_date = ''){
                $condition["create_time"] = [
                    [
                        ">",
                        strtotime($start_date)
                    ]
                ];
                $count_condition["create_time"] = [
                    [
                        ">",
                        strtotime($start_date)
                    ]
                ];
            }else if($start_date == '' && $end_date != ''){
                $condition["create_time"] = [
                    [
                        "<",
                        strtotime($end_date)
                    ]
                ];
                $condition["create_time"] = [
                    [
                        "<",
                        strtotime($end_date)
                    ]
                ];
            }
            $count_condition = '';
            $condition["id"] = [
                ">",
                0
            ];
            $shop = new Shop();
            $list = $shop->getShopOrderReturnList($pageindex, PAGESIZE, $condition, 'create_time desc');
            $count = $shop->getShopAccountSales($count_condition);
            return [
                "list" => $list,
                "count" => $count
            ];
        }
       
        //获取所有店铺列表
        $shopservice = new Shop();
        $shoplist = $shopservice ->getShopList(1,0,'','shop_id asc');
        
        
        $this->assign('shoplist',$shoplist['data']);
        return view($this->style . 'Statistics/orderStat');
    }

    /**
     * 商品分析
     *
     * @return \think\response\View
     */
    public function goodsStat()
    {
        $goods = new Goods();
        $goods_list = $goods->getGoodsRank(array());
        $this->assign("goods_list", $goods_list);
        return view($this->style . 'Statistics/goodsStat');
    }

    /**
     * 售后 分析
     *
     * @return \think\response\View
     */
    public function afterSale()
    {
        return view($this->style . 'Statistics/afterSale');
    }

    /**
     * 商品销售排行
     */
    public function goodsSaleVolumeRanking()
    {
        $platform = new Platform();
        $goods_sale_volume_ranking_array = array();
        $goods_sale_volume_ranking = $platform->getGoodsSalesVolume(array());
        foreach ($goods_sale_volume_ranking as $k => $v) {
            $goods_sale_volume_ranking_array[$v["goods_id"]] = $v;
        }
    }

    public function platformAccountRecordsList()
    {
        if (request()->isAjax()) {
            $pageindex = isset($_POST['pageIndex']) ? $_POST['pageIndex'] : 1;
            $start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
            $end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2099-1-1';
            $condition["create_time"] = [
                [
                    ">",
                    $start_date
                ],
                [
                    "<",
                    $end_date
                ]
            ];
            $platform = new Platform();
            $list = $platform->getPlatformAccountRecordsList($pageindex, PAGESIZE, $condition, 'create_time desc');
            return $list;
        } else {
            return view($this->style . 'Statistics/platformAccountRecordsList');
        }
    }

    /**
     * 商品分类销售排行榜
     */
    public function goodsCategorySaleNumRank()
    {
        return view($this->style . "Statistics/goodsCategorySaleNumRank");
    }

    /**
     * 销售分类柱形图数据
     *
     * @return multitype:multitype:unknown multitype:Ambigous <\think\static>
     */
    public function getCategorySaleNumData()
    {
        $goods_category = new GoodsCategory();
        $list = $goods_category->getGoodsCategorySaleNum();
        $category_list = array();
        $sale_num_list = array();
        foreach ($list as $k => $v) {
            $category_list[] = $v["category_name"];
            $sale_num_list[] = $v["sale_num"];
        }
        return [
            "category_list" => $category_list,
            "sale_num_list" => $sale_num_list
        ];
    }

    /**
     * 平台统计
     *
     * @return \think\response\View
     */
    public function platformAccountMonthRecored()
    {
        $platform = new Platform();
        $statistics_service = new StatisticsService();
        
        $account_count = $platform->getAccountCount();
        $integral_count = $statistics_service->getGrantIntegralSum([]);

        $this->assign('AccountCount', $account_count);
        $this->assign('integral_count',$integral_count);
        
        return view($this->style . 'Statistics/platformAccountMonthRecored');
    }
    
    /**
     * 统计数据
     * @return multitype:Ambigous <string, unknown> Ambigous <string, multitype:multitype: , number>
     */
    public function platformAccountMonthRecoredJson(){
        $platform = new Platform();
        $MonthRecored = $platform->getPlatformAccountMonthRecord();
        $month_string = '';
        $money = '';
        foreach ($MonthRecored as $k => $v){
            $month_string[]=$k;
            $money[]= $v['money'];
        }
    
        $array = [$month_string, $money];
        return $array;
    }
    /**
     * 平台账户记录
     */
    public function platformAccountRecordsCountList()
    {
        if (request()->isAjax()) {
            $pageindex = isset($_POST['pageIndex']) ? $_POST['pageIndex'] : 1;
            $start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
            $end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2099-1-1';
            $condition["create_time"] = [
                [
                    ">",
                    $start_date
                ],
                [
                    "<",
                    $end_date
                ]
            ];
            
            $platform = new Platform();
            $list = $platform->getPlatformAccountRecordsList($pageindex, PAGESIZE, $condition, 'create_time desc');
            $count = $platform->getPlatformCount($start_date, $end_date);
            return [
                "list" => $list,
                "count" => $count
            ];
        } else {
            return view($this->style . "Statistics/platformAccountRecordsCountList");
        }
    }

    /**
     * 店铺账户
     */
    public function shopAccountList()
    {
        if (request()->isAjax()) {
            $pageindex = isset($_POST['pageIndex']) ? $_POST['pageIndex'] : 1;
            $shop = new Shop();
            $condition = array();
            $list = $shop->getShopAccountCountList($pageindex, PAGESIZE, $condition, '');
            return $list;
        } else {
            return view($this->style . "Statistics/shopAccountList");
        }
    }

    /**
     * 修改平台提成比率
     * 
     * @return unknown
     */
    public function updateShopPlatformCommissionRate()
    {
        $shop = new Shop();
        $shop_id = request()->post('shop_id','');
        $shop_platform_commission_rate = request()->post('commission_rate','');
        $res = $shop->updateShopPlatformCommissionRate($shop_id, $shop_platform_commission_rate);
        return $res;
    }

    /**
     * 店铺账户明细
     */
    public function shopAccountRecordsList()
    {
        if (request()->isAjax()) {
            
            $pageindex = isset($_POST['pageIndex']) ? $_POST['pageIndex'] : 1;
            $start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '';
            $end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '';
            $account_type = ! empty($_POST['account_type']) ? $_POST['account_type'] : 0;
            $shop_id = isset($_POST["shop_id"]) ? $_POST["shop_id"] : 0;
            if ($shop_id != 0) {
                $condition["shop_id"] = $shop_id;
            }
            $condition = array();
            if ($account_type != 0) {
                $condition["account_type"] = $account_type;
            }
            if ($start_date != "") {
                $condition["create_time"][] = [
                    ">",
                    $start_date
                ];
            }
            if ($end_date != "") {
                $condition["create_time"][] = [
                    "<",
                    $end_date
                ];
            }
            $shop = new Shop();
            $list = $shop->getShopAccountRecordsList($pageindex, PAGESIZE, $condition, 'create_time desc');
            return $list;
        } else {
            $shop_id = isset($_GET["shop_id"]) ? $_GET["shop_id"] : 0;
            $this->assign("shop_id", $shop_id);
            return view($this->style . "Statistics/ShopAccountRecordsList");
        }
    }
    /**
     * 新老用户占比 统计
     */
    public function newOrOldMemberCount(){
        if (request()->isAjax()) {
            $type = request()->post("type", 1); // 1 按日统计 2 按周统计 1 按月统计
            $condition1 = array();
            $condition1["reg_time"] = ["lt", time()-86400*90];
            $condition2 = array();
            $condition2["reg_time"] = ["gt", time()-86400*90];
            if($type == 1){
                // 查询今天
                $start = strtotime(date('Y-m-d 00:00:00'));
                $end = strtotime(date('Y-m-d H:i:s'));
            }else if($type == 2){
                $start = mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y"));
                $end = mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y"));
            }else if($type == 3){
                $start = mktime(0, 0 , 0,date("m"),1,date("Y"));
                $end = mktime(23,59,59,date("m"),date("t"),date("Y"));
            }
            $order_condition["create_time"] = [
                'between',
                [
                    $start,
                    $end
                ]
            ];
            $statistics_service = new StatisticsService();
            $old_num = $statistics_service -> getMemberUseNum($condition1, $order_condition);
            $new_num = $statistics_service -> getMemberUseNum($condition2, $order_condition);
//             $old_num = $statistics_service -> getMemberUseMoney($condition1, $order_condition);//老用户用户销量查询
//             $new_num = $statistics_service -> getMemberUseMoney($condition2, $order_condition);//新用户销量查询
            $count_num = $old_num + $new_num;
            if($count_num > 0){
                $old_ratio = $old_num / $count_num;
                $new_ratio = $old_num / $count_num;
            }
         }else{
             return view($this->style . "Statistics/newOrOldMemberCount");
        }
        
    }
    
    
    /**
     * 商品销售排行
     *
     * @return unknown
     */
    public function getGoodsRealSalesRank($condition=array())
    {
        $goods = new Goods();
        $goods_list = $goods->getGoodsRank($condition);
        return $goods_list;
    }    
    
    /**
     * 店铺商品销售排行
     *
     * @return unknown
     */
    public function getShopRealSalesRank()
    {
        $goods = new Goods();
        
        $condition['shop_id'] = array('gt', 0);
        $group = 'shop_id';
        $order = 'shop_sales';
        $limit = 6; 
        $field = 'shop_id, sum(sales) as shop_sales';
        $shop_list = $goods->getShopGoodsRank($condition, $group, $order, $limit, $field);

        foreach ($shop_list as $value) {
            $shop = new Shop();
            $shop_id = $value['shop_id'];
            $value['shop_name'] = $shop->getShopInfo($shop_id, 'shop_name')['shop_name'];
        }
        return $shop_list;
    }  
    
    /**
     * 平台下单量/下单金额图标数据
     *
     * @return Ambigous <multitype:, unknown>
     */
    public function getShopOrderChartCount()
    {
        
        $type = request()->post('type',1);// 按月、周、日查询
        $year = request()->post('year');
        $month = request()->post('month');
        $week = request()->post('week');
        $day = request()->post('day');
        $var_type = request()->post('var_type',1);// 查询下单量、下单金额
        
        $order = new Order();
        $data = array();
        if ($type == 1) {

            //list ($start, $end) = Time::today();
            $start = getTimeTurnTimeStamp($day);

            for ($i = 0; $i < 24; $i ++) {
                $date_start = date("Y-m-d H:i:s", $start + 3600 * $i);
                $date_end = date("Y-m-d H:i:s", $start + 3600 * ($i + 1));
                $condition = [
                    'create_time' => [
                        'between',
                        [
                            getTimeTurnTimeStamp($date_start),
                            getTimeTurnTimeStamp($date_end)
                        ]
                    ],
                    "order_status" => [
                        'NEQ',
                        0
                    ],
                    "order_status" => [
                        'NEQ',
                        5
                    ]
                ];
                $count = $this->getShopSaleData($condition, $var_type);
    
                $data[0][$i] = $i . ':00';
                $data[1][$i] = $count;
            }
        } else
            if ($type == 2) {
                list ($start, $end) = Time::yesterday();
                for ($j = 0; $j < 24; $j ++) {
                    $date_start = date("Y-m-d H:i:s", $start + 3600 * $j);
                    $date_end = date("Y-m-d H:i:s", $start + 3600 * ($j + 1));
                    $condition = [
                        //'shop_id' => $this->instance_id,
                        'create_time' => [
                            'between',
                            [
                                getTimeTurnTimeStamp($date_start),
                                getTimeTurnTimeStamp($date_end)
                            ]
                        ],
                        
                        "order_status" => [
                            'NEQ',
                            5
                        ]
                    ];
                    $count = $this->getShopSaleData($condition, $var_type);
                    $data[0][$j] = $j . ':00';
                    $data[1][$j] = $count;
                }
            } else
                if ($type == 3) {
                    //$start = strtotime(date('Y-m-d 00:00:00', strtotime('last day this week + 1 day')));
                    
                    $start = getTimeTurnTimeStamp($week);
                    
                    for ($j = 0; $j < 7; $j ++) {
                        $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                        $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                        $condition = [
                            'create_time' => [
                                'between',
                                [
                                    getTimeTurnTimeStamp($date_start),
                                    getTimeTurnTimeStamp($date_end)
                                ]
                            ],
                            
                            "order_status" => [
                                'NEQ',
                                5
                            ]
                        ];
                        $count = $this->getShopSaleData($condition, $var_type);
                        
                        $data[0][$j] = '星期' . numChangeWeek($j + 1);
                        $data[1][$j] = $count;
                    }
                } else
                    if ($type == 4) {
                        //list ($start, $end) = Time::month();
                        $month_start = $year."-".$month."-01";
                        $month_end =  date("Y-m-d",strtotime("$month_start + 1 month - 1 day"));
                        
                        $start = getTimeTurnTimeStamp($month_start);
                        $end = getTimeTurnTimeStamp($month_end);
                        
                        for ($j = 0; $j < ($end + 1 - $start) / 86400; $j ++) {
                            $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                            $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                            $condition = [
                                'create_time' => [
                                    'between',
                                    [
                                        getTimeTurnTimeStamp($date_start),
                                        getTimeTurnTimeStamp($date_end)
                                    ]
                                ],
                                
                                "order_status" => [
                                    'NEQ',
                                    5
                                ]
                            ];
                            $count = $this->getShopSaleData($condition, $var_type);
                            $data[0][$j] = (1 + $j) . '日';
                            $data[1][$j] = $count;
                        }
                    }
                return $data;
    }
    
    /**
     * 查询一段时间内的总下单量及下单金额
     *
     * @return multitype:\app\admin\controller\Ambigous Ambigous <\app\admin\controller\Ambigous, number, \data\service\niushop\unknown, \data\service\niushop\Order\unknown, unknown>
     */
    public function getOrderShopSaleCount()
    {
        $date = request()->post('date',1);
        $time = request()->post('time',1);
        
        // 查询一段时间内的下单量及下单金额
        if ($date == 1) {
            
            if(!empty($time)){
                $start = getTimeTurnTimeStamp($time);
                $end = $start + 3600 * 24;
            }else{
                list ($start, $end) = Time::today();
            }
            
            $start_date = date("Y-m-d H:i:s", $start);
            $end_date = date("Y-m-d H:i:s", $end);
        } else
            if ($date == 3) {
                $start_date = date('Y-m-d 00:00:00', strtotime('last day this week + 1 day'));
                $end_date = date('Y-m-d 00:00:00', strtotime('last day this week +8 day'));
            } else
                if ($date == 4) {
                    list ($start, $end) = Time::month();
                    $start_date = date("Y-m-d H:i:s", $start);
                    $end_date = date("Y-m-d H:i:s", $end);
                }
            $condition = array();
            $condition["shop_id"] = $this->instance_id;
            $condition["shop_id"];
            $condition["create_time"] = [
                'between',
                [
                    getTimeTurnTimeStamp($start_date),
                    getTimeTurnTimeStamp($end_date)
                ]
            ];
            $count_money = $this->getShopSaleData($condition, 1);
            $count_num = $this->getShopSaleData($condition, 2);
            return array(
                "count_money" => $count_money,
                "count_num" => $count_num
            );
    }
    
    /**
     * 下单量/下单金额 数据
     *
     * @param unknown $condition
     * @param unknown $type
     * @return Ambigous <\data\service\niushop\Ambigous, \data\service\niushop\Order\unknown, number, unknown>
     */
    public function getShopSaleData($condition, $type)
    {
        $order = new Order();
        if ($type == 1) {
            $count = $order->getShopSaleSum($condition);
            $count = (float) sprintf('%.2f', $count);
        } else {
            $count = $order->getShopSaleNumSum($condition);
        }
        return $count;
    }
    /**
     * 会员积分趋势图
     */
    public function memberIntegralCount(){
        $statistics_service = new StatisticsService();
        $date = request()->post('date',1);
        $data = array();
        if ($date == 1) {
            list ($start, $end) = Time::today();
            for ($i = 0; $i < 24; $i ++) {
                $date_start = date("Y-m-d H:i:s", $start + 3600 * $i);
                $date_end = date("Y-m-d H:i:s", $start + 3600 * ($i + 1));
                $condition = [
                    'create_time' => [
                        'between',
                        [
                            getTimeTurnTimeStamp($date_start),
                            getTimeTurnTimeStamp($date_end)
                        ]
                    ]
                ];
                $count = $statistics_service->getGrantIntegralSum($condition);
        
                $data[0][$i] = $i . ':00';
                $data[1][$i] = $count;
            }
        } else if ($date == 3) {
            $start = strtotime(date('Y-m-d 00:00:00', strtotime('last day this week + 1 day')));
            for ($j = 0; $j < 7; $j ++) {
                $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                $condition = [
                    'create_time' => [
                        'between',
                        [
                            getTimeTurnTimeStamp($date_start),
                            getTimeTurnTimeStamp($date_end)
                        ]
                    ]
                ];
                $count = $statistics_service->getGrantIntegralSum($condition);
                $data[0][$j] = '星期' . numChangeWeek($j + 1);
                $data[1][$j] = $count;
            }
        } else
            if ($date == 4) {
                list ($start, $end) = Time::month();
                for ($j = 0; $j < ($end + 1 - $start) / 86400; $j ++) {
                    $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                    $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                    $condition = [
                        'create_time' => [
                            'between',
                            [
                                getTimeTurnTimeStamp($date_start),
                                getTimeTurnTimeStamp($date_end)
                            ]
                        ]
                    ];
                    $count = $statistics_service->getGrantIntegralSum($condition);
                    $data[0][$j] = (1 + $j) . '日';
                    $data[1][$j] = $count;
                }
            }
        return $data;
    }
    /**
     * 会员余额趋势图
     * @return Ambigous <multitype:, number, \data\model\unknown, unknown>
     */
    public function memberBalanceCount(){
        $statistics_service = new StatisticsService();
        $date = request()->post('date',1);
        $data = array();
        if ($date == 1) {
            list ($start, $end) = Time::today();
            for ($i = 0; $i < 24; $i ++) {
                $date_start = date("Y-m-d H:i:s", $start + 3600 * $i);
                $date_end = date("Y-m-d H:i:s", $start + 3600 * ($i + 1));
                $condition = [
                    'create_time' => [
                        'between',
                        [
                            getTimeTurnTimeStamp($date_start),
                            getTimeTurnTimeStamp($date_end)
                        ]
                    ]
                ];
                $count = $statistics_service->getGrantBalanceSum($condition);
        
                $data[0][$i] = $i . ':00';
                $data[1][$i] = $count;
            }
        } else if ($date == 3) {
                $start = strtotime(date('Y-m-d 00:00:00', strtotime('last day this week + 1 day')));
                for ($j = 0; $j < 7; $j ++) {
                    $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                    $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                    $condition = [
                        'create_time' => [
                            'between',
                            [
                                getTimeTurnTimeStamp($date_start),
                                getTimeTurnTimeStamp($date_end)
                            ]
                        ]
                    ];
                    $count = $statistics_service->getGrantBalanceSum($condition);
                    $data[0][$j] = '星期' . numChangeWeek($j + 1);
                    $data[1][$j] = $count;
                }
            } else
                if ($date == 4) {
                    list ($start, $end) = Time::month();
                    for ($j = 0; $j < ($end + 1 - $start) / 86400; $j ++) {
                        $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                        $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                        $condition = [
                            'create_time' => [
                                'between',
                                [
                                    getTimeTurnTimeStamp($date_start),
                                    getTimeTurnTimeStamp($date_end)
                                ]
                            ]
                        ];
                        $count = $statistics_service->getGrantBalanceSum($condition);
                        $data[0][$j] = (1 + $j) . '日';
                        $data[1][$j] = $count;
                    }
         }
         return $data;
    }
    /**
     * 会员注册趋势图
     * @return Ambigous <multitype:, number, \data\model\unknown, unknown>
     */
    public function memberRegisterCount(){
        $statistics_service = new StatisticsService();
        $date = request()->post('date',1);
        $data = array();
        if ($date == 1) {
            list ($start, $end) = Time::today();
            for ($i = 0; $i < 24; $i ++) {
                $date_start = date("Y-m-d H:i:s", $start + 3600 * $i);
                $date_end = date("Y-m-d H:i:s", $start + 3600 * ($i + 1));
                $condition = [
                    'reg_time' => [
                        'between',
                        [
                            getTimeTurnTimeStamp($date_start),
                            getTimeTurnTimeStamp($date_end)
                        ]
                    ]
                ];
                $count = $statistics_service->getMemberCount($condition);
    
                $data[0][$i] = $i . ':00';
                $data[1][$i] = $count;
            }
        } else if ($date == 3) {
            $start = strtotime(date('Y-m-d 00:00:00', strtotime('last day this week + 1 day')));
            for ($j = 0; $j < 7; $j ++) {
                $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                $condition = [
                    'reg_time' => [
                        'between',
                        [
                            getTimeTurnTimeStamp($date_start),
                            getTimeTurnTimeStamp($date_end)
                        ]
                    ]
                ];
                $count = $statistics_service->getMemberCount($condition);
                $data[0][$j] = '星期' . numChangeWeek($j + 1);
                $data[1][$j] = $count;
            }
        } else
            if ($date == 4) {
                list ($start, $end) = Time::month();
                for ($j = 0; $j < ($end + 1 - $start) / 86400; $j ++) {
                    $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                    $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                    $condition = [
                        'reg_time' => [
                            'between',
                            [
                                getTimeTurnTimeStamp($date_start),
                                getTimeTurnTimeStamp($date_end)
                            ]
                        ]
                    ];
                    $count = $statistics_service->getMemberCount($condition);
                    $data[0][$j] = (1 + $j) . '日';
                    $data[1][$j] = $count;
                }
            }
        return $data;
    }
    /**
     * 会员注册记录
     */
    public function memberRegRecord(){
        if (request()->isAjax()) {
    
        }else{
            ob_clean();
            return view($this->style . "Statistics/memberRegRecord");
        }
    }
    /**
     * 订单相关内容统计
     */
    public function getOrderInfoCount(){
        
        $statistics_service = new StatisticsService();
        $start_date = request()->post('start_date', "");
        $end_date = request()->post('end_date', '');
        $shop_id = request()->post('shop_id', '');
        $condition = array();
        if($shop_id != ""){
            $condition["shop_id"] = $shop_id;
        }
        if($start_date != '' && $end_date != ''){
            $condition["create_time"] = [
                [
                    ">",
                    strtotime($start_date)
                ],
                [
                    "<",
                    strtotime($end_date)
                ]
            ];
        }else if($start_date != '' && $end_date = ''){
            $condition["create_time"] = [
                [
                    ">",
                    strtotime($start_date)
                ]
            ];
        }else if($start_date == '' && $end_date != ''){
            $condition["create_time"] = [
                [
                    "<",
                    strtotime($end_date)
                ]
            ];
        }
        //一段时间内的交易额以及交易量
        $order_num = $statistics_service->getOrderCount($condition); //交易量
        $order_money = $statistics_service->getOrderMoney($condition); //交易额
        return array("order_num" => $order_num , "order_money" => $order_money);
    }
    /**
     * 获取会员注册列表
     * @return multitype:number unknown
     */
    public function userRegRecordsList()
    {
    	$statistics = new StatisticsService();
    	if (request()->isAjax()) {
    		$page_index = request()->post('pageIndex',1);
    		$page_size = request()->post('page_size',PAGESIZE);
    		$search_text = request()->post('search_text', '');
    		$start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
    		$end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2030-1-1';
    		if ($start_date != "") {
    			$condition["reg_time"][] = [
    					">",
    					getTimeTurnTimeStamp($start_date)
    			];
    		}
    		if ($end_date != "") {
    			$condition["reg_time"][] = [
    					"<",
    					getTimeTurnTimeStamp($end_date)
    
    			];
    		}
    		$list = $statistics->getMemberRegRecordsList($page_index, $page_size, $condition, 'reg_time desc', 'uid,user_name,user_status,user_tel,user_email,user_email,reg_time');
    
    		return $list;
    	}
    
    }
    
    /**
     * 会员余额列表
     * @return multitype:number unknown |\think\response\View
     */
    public function memberBalanceList()
    {
    	ob_clean();
    	return view($this->style . "Statistics/memberBalanceList");
    }
    /**
     * 会员积分列表
     * @return multitype:number unknown |\think\response\View
     */
    public function memberPointList()
    {
    	ob_clean();
    	return view($this->style . "Statistics/memberPointList");
    }
    /**
     * 会员账户明细
     */
    public function memberAccountRecords()
    {
    	$statistics = new StatisticsService();
    	if (request()->isAjax()) {
    	    $page_index = request()->post("page_index", 1);
    	    $page_size = request()->post('page_size', PAGESIZE);
    	    $search_text = request()->post('search_text', '');
    	    $form_type = request()->post('form_type');
    		$type = request()->post('type',1);
    		$start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
    		$end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2030-1-1';
    		
    		if ($form_type != '') {
    		    $condition['from_type'] = $form_type;
    		}
    		
    		if ($start_date != "") {
    			$condition["create_time"][] = [
    					">",
    					getTimeTurnTimeStamp($start_date)
    			];
    		}
    		if ($end_date != "") {
    			$condition["create_time"][] = [
    					"<",
    					getTimeTurnTimeStamp($end_date)
    
    			];
    		}
    		$condition['account_type'] = $type;
    		$list = $statistics->getMemberBalanceRecordsList($page_index, $page_size, $condition, 'create_time desc', '');
    		if($type == 1){
    			$count = $statistics->getGrantIntegralSum($condition);
    		}elseif ($type == 2){
    			$count = $statistics->getGrantBalanceSum($condition);
    		}
    		 
    		return [
    				'count'	=> $count,
    				'list'	=> $list,
    		];
    	}
    }
    
    /*
     * 平台总营业额对应明细表
     */
    function platformAccountTotalRecordList(){             
        $statistics = new StatisticsService();
        if(request()->isAjax()){
            $page_index = request()->post('pageIndex',1);
            $page_size = request()->post('page_size',PAGESIZE);
            $shop_id = request()->post('shop_id');
            $issue = request()->post('issue', '');
            
            if($shop_id != ''){
                $condition['nsor.shop_id'] = $shop_id;
            }
            
            if($issue != ''){
                $condition = ['is_issue' => $issue];
            }
            
            $start_date = request()->post("start_date",'2010-1-1' );
            $end_date = request()->post("start_date",'2030-1-1' );
            
            if ($start_date != "") {
                $condition["nsor.create_time"][] = [
                    ">",
                    getTimeTurnTimeStamp($start_date)
                ];
            }
            if ($end_date != "") {
                $condition["nsor.create_time"][] = [
                    "<",
                    getTimeTurnTimeStamp($end_date)
                ];
            }
            
            $list = $statistics->getOrderReturnRecordList($page_index, $page_size, $condition, 'nsor.create_time desc', '');
            return $list;
        }else{
            ob_clean();
            //获取所有店铺列表
            $shopservice = new Shop();
            $shoplist = $shopservice ->getShopList(1,0,'','shop_id asc');
            $this->assign('shoplist',$shoplist['data']);
            $issue = request()->get('issue', '');
            $this->assign('issue',$issue);
            return view($this->style . 'Statistics/platformAccountTotalRecordList');
        } 
    }
    
    /*
     * 平台订单总额对应明细表
     */
    function platformAccountOrderRecordList(){
    
        $statistics = new StatisticsService();
        if(request()->isAjax()){
            $page_index = request()->post('pageIndex',1);
            $page_size = request()->post('page_size',PAGESIZE);
            $shop_id = request()->post('shop_id');
            
            $condition = ['nsor.is_issue' => 1];
            
            if($shop_id != ''){
                $condition['nsor.shop_id'] = $shop_id;
            }
            
            $start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
            $end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2030-1-1';
    
            if ($start_date != "") {
                $condition["nsor.create_time"][] = [
                    ">",
                    getTimeTurnTimeStamp($start_date)
                ];
            }
            if ($end_date != "") {
                $condition["nsor.create_time"][] = [
                    "<",
                    getTimeTurnTimeStamp($end_date)
                     
                ];
            }
    
            $list = $statistics->getOrderReturnRecordList($page_index, $page_size, $condition, 'nsor.create_time desc', '');
            return [
                'list' => $list   
            ];
        }else{
            ob_clean();
            //获取所有店铺列表
            $shopservice = new Shop();
            $shoplist = $shopservice ->getShopList(1,0,'','shop_id asc');
            
            $this->assign('shoplist',$shoplist['data']);
            return view($this->style . 'Statistics/platformAccountOrderRecordList');
        }
    }
    
    /**
     * 平台利率
     */
    public function accountReturnRecordsList()
    {
    	$statistics = new StatisticsService();
    	if (request()->isAjax()) {
    		$page_index = request()->post('pageIndex',1);
    		$page_size = request()->post('page_size',PAGESIZE);
    		$search_text = request()->post('search_text', '');
    		$shop_id = request()->post('shop_id');
    		
    		if($shop_id != ''){
    		    $condition['shop_id'] = $shop_id;
    		}
    		
    		$start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
    		$end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2030-1-1';
    		if ($start_date != "") {
    			$condition["create_time"][] = [
    					">",
    					getTimeTurnTimeStamp($start_date)
    			];
    		}
    		if ($end_date != "") {
    			$condition["create_time"][] = [
    					"<",
    					getTimeTurnTimeStamp($end_date)
    	
    			];
    		}
    		$list = $statistics->getPlatformAccountRecords($page_index, $page_size, $condition, 'create_time desc', '');
    		return [
				'list'	=> $list,
    		];
    	}else{
    		ob_clean();
    		$shopservice = new Shop();
    		$shoplist = $shopservice ->getShopList(1,0,'','shop_id asc');
    		
    		$this->assign('shoplist',$shoplist['data']);
    		return view($this->style . "Statistics/accountReturnRecordsList");
    	}
    }

    
    /**
     * 店铺提现
     */
    public function shopAccountWithdrawList()
    {
    	$statistics = new StatisticsService();
    	if (request()->isAjax()) {
    		$page_index = request()->post('pageIndex',1);
    		$page_size = request()->post('page_size',PAGESIZE);
    		$search_text = request()->post('search_text', '');
    		$shop_id = request()->post('shop_id');
    		
    		if($shop_id != ''){
    		    $condition['shop_id'] = $shop_id;
    		}
    		
    		$start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
    		$end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2030-1-1';
    		if ($start_date != "") {
    			$condition["create_time"][] = [
    					">",
    					getTimeTurnTimeStamp($start_date)
    			];
    		}
    		if ($end_date != "") {
    			$condition["create_time"][] = [
    					"<",
    					getTimeTurnTimeStamp($end_date)
    					 
    			];
    		}
    		$list = $statistics->getShopAccountWithdrawList($page_index, $page_size, $condition, 'create_time desc', '');
    		return [
				'list'	=> $list,
    		];
    	}else{
    		ob_clean();
    		$shopservice = new Shop();
    		$shoplist = $shopservice ->getShopList(1,0,'','shop_id asc');
    		
    		$this->assign('shoplist',$shoplist['data']);
    		return view($this->style . "Statistics/shopAccountWithdrawList");
    	}
    }
    
    /**
     * 店铺余额
     */
    public function shopAccountProceedsList()
    {
    	$statistics = new StatisticsService();
    	if (request()->isAjax()) {
    		$page_index = request()->post('pageIndex',1);
    		$page_size = request()->post('page_size',PAGESIZE);
    		$search_text = request()->post('search_text', '');
    		$shop_id = request()->post('shop_id');
    		
    		$condition = ['nsor.is_issue' => 0];
    		if($shop_id != ''){
    		    $condition['nsor.shop_id'] = $shop_id;
    		}
    		
    		$start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
    		$end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2030-1-1';
    		if ($start_date != "") {
    			$condition["nsor.create_time"][] = [
    					">",
    					getTimeTurnTimeStamp($start_date)
    			];
    		}
    		if ($end_date != "") {
    			$condition["nsor.create_time"][] = [
    					"<",
    					getTimeTurnTimeStamp($end_date)
    
    			];
    		}
    		$list = $statistics->getOrderReturnRecordList($page_index, $page_size, $condition, 'nsor.create_time desc', '');
    		return [
				'list'	=> $list,
    		];
    	}else{
    		ob_clean();
    		$shopservice = new Shop();
    		$shoplist = $shopservice ->getShopList(1,0,'','shop_id asc');
    		
    		$this->assign('shoplist',$shoplist['data']);
    		return view($this->style . "Statistics/shopAccountProceedsList");
    	}
    }
    
    /**
     * 平台待结算利润
     */
    public function platformWaitProfitList(){
        $statistics = new StatisticsService();
        if (request()->isAjax()) {
            $page_index = request()->post('pageIndex',1);
            $page_size = request()->post('page_size',PAGESIZE);
            $search_text = request()->post('search_text', '');
            $shop_id = request()->post('shop_id');
            
            $condition = ['nsor.is_issue' => 0];
            if($shop_id != ''){
                $condition['nsor.shop_id'] = $shop_id;
            }
            
            $start_date = ! empty($_POST['start_date']) ? $_POST['start_date'] : '2010-1-1';
            $end_date = ! empty($_POST['end_date']) ? $_POST['end_date'] : '2030-1-1';
            if ($start_date != "") {
                $condition["nsor.create_time"][] = [
                    ">",
                    getTimeTurnTimeStamp($start_date)
                ];
            }
            if ($end_date != "") {
                $condition["nsor.create_time"][] = [
                    "<",
                    getTimeTurnTimeStamp($end_date)
            
                ];
            }
            $list = $statistics->getOrderReturnRecordList($page_index, $page_size, $condition, 'nsor.create_time desc', '');
            return $list;
        }else{
            $shopservice = new Shop();
            $shoplist = $shopservice ->getShopList(1,0,'','shop_id asc');
            
            $this->assign('shoplist',$shoplist['data']);
            return view($this->style . "Statistics/platformWaitProfitList");            
        }
    }
}
