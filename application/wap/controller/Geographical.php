<?php
/**
 * Order.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\wap\controller;

use data\service\Address;
use \think\Session as Session;
use think\Controller;

/**
 * 地理位置控制器
 *
 * @author Administrator
 *        
 */
class Geographical extends BaseController
{
    public function index(){
        $this->getCityMobileItem();
        return view($this->style . 'Geographical/index');
    }
    
    /**
     * 移动端城市集合
     */
    public function getCityMobileItem(){
        
        $latter_item = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        $latter_array = array();
        foreach($latter_item as $k => $v){
            $latter_array[$v] = [];
        }
        $hot_city_item = array();
        $address_service = new Address();
        $city_list = $address_service ->getCityList(0);
        foreach($city_list as $k => $v){
            $key_item = cityFirstPinyin($v["city_name"]);
            if(empty($latter_array[$key_item])){
                $hot_city_item[] = $v["city_name"];
            }
            $latter_array[$key_item][] = $v["city_name"];
        }
        $this->assign("city_item", $latter_array);
        $this->assign("latter_item", $latter_item);
        $this->assign("hot_city_item", $hot_city_item);
    }
    
    
    /**
     * 检测城市是否存在session
     */
    public function getCitySesson(){
        $city = Session::get('city_session');
        return $city;
    }
    /**
     * 设置城市session
     */
    public function SetCitySesson(){
        $city = request()->post("city", "");
        Session::set('city_session', $city);
    }
    
    /**
     * 获取城市集合
     */
    public function getCityItem(){
        $address_service = new Address();
        $city_list = $address_service ->getCityList(0);
        $list = array();
        foreach($city_list as $k => $v){
            $list[] = cityChineseTranPinyin($v["city_name"]);
        }
        return $list;
    }
}