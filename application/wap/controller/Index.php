<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\wap\controller;

use data\service\Config;
use data\service\Goods;
use data\service\GoodsBrand as GoodsBrand;
use data\service\GoodsCategory;
use data\service\Member;
use data\service\Member as MemberService;
use data\service\Platform;
use data\service\promotion\PromoteRewardRule;
use data\service\WebSite;
use think\Cookie;
use data\service\Shop;
use data\service\Promotion;
use think\Cache;

class Index extends BaseController
{

    /**
     * 商品楼层板块每层显示商品个数
     *
     * @var unknown
     */
    public $category_good_num = 4;

    /**
     * 商品标签板块每层显示商品个数
     *
     * @var unknown
     */
    public $recommend_goods_num = 4;

    /**
     * 手机端首页
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function index()
    {
       /*  $default_client = request()->cookie("default_client", "");
        if(!request()->isMobile()&&$default_client == "")
        {
            $redirect = __URL(__URL__);
            $this->redirect($redirect);
            exit();
        } */
        //通过汇率获取人民币价格显示
        $mypayrate = Cache::get("mypayrate");
        // 分享
        $ticket = $this->getShareTicket();
        $this->assign("signPackage", $ticket);
       
        
        //首页导航
        $shop = new Shop();
        $navigation_list = $shop ->ShopNavigationList(1, 0, ['type'=>2, 'is_show'=>1], "sort");
        $this->assign("navigation_list", $navigation_list["data"]);

        // 首页轮播图
        $platform = new Platform();
        $plat_adv_list = $platform->getPlatformAdvPositionDetail(1105);
        $this->assign('plat_adv_list', $plat_adv_list);
        // 首页新品推荐下方广告位
        $index_adv_one = $platform->getPlatformAdvPositionDetail(1188);
        $this->assign('index_adv_one', $index_adv_one);
        // 首页品牌推荐下方广告位
        $index_adv_two = $platform->getPlatformAdvPositionDetail(1189);
        $this->assign('index_adv_two', $index_adv_two);
        // 首页楼层版块
        $good_category = new GoodsCategory();
        //$block_list = $good_category->getGoodsCategoryBlockList(0);
        $good_category = new GoodsCategory();
        $shop_id = 0;
        
        // 首页楼层版块
        $block_list = $good_category->getGoodsCategoryBlockQuery($shop_id, $this->category_good_num);

        $this->assign('block_list', $block_list);
        // 首页新品推荐列表
        $goods_platform = new Platform();
        $shop_id = $this->instance_id;
        $goods_platform_list = $goods_platform->getRecommendGoodsList($shop_id, $this->recommend_goods_num);
        $this->assign('goods_platform_list', $goods_platform_list);
        
        // 品牌列表
        $goods_brand = new GoodsBrand();
        $list = $goods_brand->getGoodsBrandList(1, 6, '', 'sort');
        $this->assign('list', $list['data']);
        
        // 限时折扣列表
        $goods = new Goods();
        $condition['status'] = 1;
        $condition['ng.state'] = 1;
        $discount_list = $goods->getDiscountGoodsList(1, 0, $condition, 'end_time');
        
        foreach ($discount_list['data'] as $k => $v) {
            $v['discount'] = str_replace('.00', '', $v['discount']);
            // $v['promotion_price'] = str_replace('.00', '', $v['promotion_price']);
            // $v['price'] = str_replace('.00', '', $v['price']);
            $rmb_promotion_price =number_format($discount_list['data'][$k]['promotion_price']*$mypayrate,'2');
            //市场价
            $discount_list['data'][$k]['rmb_promotion_price'] = $rmb_promotion_price;
        }
        
        $this->assign('discount_list', $discount_list['data']);
        // 获取当前时间
        $current_time = $this->getCurrentTime();
        $this->assign('ms_time', $current_time);
        
        // 首页商城热卖
        $val['is_hot'] = 1;
        $goods_hot_list = $goods_platform->getPlatformGoodsList(1, 0, $val);
        
        $this->assign('goods_hot_list', $goods_hot_list['data']);
        // $hot_selling_adv = $platform->getPlatformAdvPositionDetail(1164);
        // $this->assign('hot_selling_adv', $hot_selling_adv);
        
        // 首页商城推荐
        $val1['is_recommend'] = 1;
        $val['nss.shop_state'] = 1;
        $goods_recommend_list = $goods_platform->getPlatformGoodsList(1, 0, $val1);
        
        $this->assign('goods_recommend_list', $goods_recommend_list['data']);
        
        // 公众号配置查询
        $config = new Config();
        $wchat_config = $config->getInstanceWchatConfig($this->instance_id);
        
        $is_subscribe = 0; // 标识：是否显示顶部关注 0：[隐藏]，1：[显示]
                           // 检查是否配置过微信公众号
        if (! empty($wchat_config['value'])) {
            if (! empty($wchat_config['value']['appid']) && ! empty($wchat_config['value']['appsecret'])) {
                // 如何判断是否关注
                if (isWeixin()) {
                    if (! empty($this->uid)) {
                        // 检查当前用户是否关注
                        $user_sub = $this->user->checkUserIsSubscribeInstance($this->uid, $this->instance_id);
                        if ($user_sub == 0) {
                            // 未关注
                            $is_subscribe = 1;
                        }
                    }
                }
            }
        }
        
        $this->assign("is_subscribe", $is_subscribe);
        // 公众号二维码获取
        $this->web_site = new WebSite();
        $web_info = $this->web_site->getWebSiteInfo();
        $this->assign('web_info', $web_info);
        $member = new MemberService();
        $source_user_name = "";
        $source_img_url = "";
        $source_uid = request()->get('source_uid', '');
        if (! empty($source_uid)) {
            $_SESSION['source_uid'] = $source_uid;
            $user_info = $member->getUserInfoByUid($_SESSION['source_uid']);
            if (! empty($user_info)) {
                $source_user_name = $user_info["nick_name"];
                if (! empty($user_info["user_headimg"])) {
                    $source_img_url = $user_info["user_headimg"];
                }
            }
        }
        // 首页公告
        // 公告
//         $platform = new Platform();
//         $notice = $platform->getNoticeList(1, '', [
//             "shop_id" => 0
//         ], "sort");
//         $this->assign("notice", $notice["data"]);
        $notice_arr = $config->getNotice(0);
        $this->assign('notice', $notice_arr);
        $this->assign('source_user_name', $source_user_name);
        $this->assign('source_img_url', $source_img_url);
        
        // 首页优惠券
        $member = new Member();
        $coupon_list = $member->getMemberCouponTypeList($this->instance_id, $this->uid);
        $this->assign('coupon_list', $coupon_list);
        
        // 游戏活动
        $promotion = new Promotion();
        $gameList = $promotion->getPromotionGamesList(1, 0, [
            'status' => 1,
            "activity_images" => [
                "neq",
                ""
            ]
        ], 'game_id desc');
        $this->assign("gameList", $gameList);
        
        $recommend_goods_list = $this->getAdminRecommendGoodsList();
        $this->assign("recommend_goods_list", $recommend_goods_list);

        // 判断是否开启了自定义模块
        if ($this->custom_template_is_enable == 1) {
            // 获取自定义模板信息
            $this->redirect(__URL(\think\Config::get('view_replace_str.APP_MAIN') . "/CustomTemplate/customTemplateIndex"));
        } else {
            
            return view($this->style . 'Index/index');
        }
    }

    /**
     * 得到当前时间戳的毫秒数
     *
     * @return number
     */
    public function getCurrentTime()
    {
        $time = time();
        $time = $time * 1000;
        return $time;
    }

    /**
     * 自定义模板界面
     * 创建时间：2017年8月14日 16:54:36
     *
     * @return \think\response\View
     */
    public function customTemplateControl()
    {
        $id = request()->get("id", "");
        $config = new Config();
        $custom_template_info = [];
        $this->assign("custom_template", $custom_template_info);
        return view($this->style . 'Index/customTemplateControl');
    }

    /**
     * 限时折扣
     */
    public function discount()
    {
        $platform = new Platform();
        $shop_id = request()->get('shop_id','');
        if($shop_id == ''){
            $shop_id = -1;
        }
        //通过汇率获取人民币价格显示
        $mypayrate = Cache::get("mypayrate");

        // 限时折扣广告位
        $discounts_adv = $platform->getPlatformAdvPositionDetail(1163);
        $this->assign('discounts_adv', $discounts_adv);
        if (request()->isAjax()) {
            $goods = new Goods();
            $category_id = request()->get('category_id', '0');
            $page_index = request()->get("page",1);
            $condition['status'] = 1;
            $condition['ng.state'] = 1;
            $shop_id =  request()->get('shop_id','');
            if(intval($shop_id )> -1){

                $condition['shop_id'] = $shop_id;
            }

            if (! empty($category_id)) {
                $condition['category_id_1'] = $category_id;
            }
            $discount_list = $goods->getDiscountGoodsList($page_index, PAGESIZE, $condition, "ng.sort asc");
            foreach ($discount_list['data'] as $k => $v) {
                $v['rmb_promotion_price'] = number_format($v['promotion_price']*$mypayrate,'2');
                $v['rmb_market_price'] = number_format($v['market_price']*$mypayrate,'2');
                $v['discount'] = str_replace('.00', '', $v['discount']);
                $v['promotion_price'] = $v['promotion_price'];
                $v['price'] = str_replace('.00', '', $v['price']);
            }
            return $discount_list;
        } else {
            $goods_category = new GoodsCategory();
            $goods_category_list_1 = $goods_category->getGoodsCategoryList(1, 0, [
                "is_visible" => 1,
                "level" => 1
            ]);

            // 获取当前时间
            $current_time = $this->getCurrentTime();
            $this->assign('ms_time', $current_time);
            $this->assign('goods_category_list_1', $goods_category_list_1['data']);
            $this->assign("title_before","限时折扣");
            $this->assign("shop_id","$shop_id");
            return view($this->style . 'Index/discount');
        }
    }
    
    // 分享送积分
    public function shareGivePoint()
    {
        if (request()->isAjax()) {
            $rewardRule = new PromoteRewardRule();
            $url = request()->post('share_url', '');
            $url_arr = parse_url($url);
            if (stristr($url_arr['path'], 'goods/goodsdetail')) {
                
                $url_query_arr = explode('&', $url_arr['query']);
                $params = array();
                foreach ($url_query_arr as $param) {
                    $item = explode('=', $param);
                    $params[$item[0]] = $item[1];
                }
                if (! empty($params['id'])) {
                    hook('pointShareGoods', [
                        'goods_id' => $params['id']
                    ]);
                }
            }
            $res = $rewardRule->memberShareSendPoint($this->instance_id, $this->uid);
            return AjaxReturn($res);
        }
    }

    /**
     * 设置页面打开cookie
     */
    public function setClientCookie()
    {
        $client = request()->post('client', '');
        Cookie::set("default_client", $client);
        $cookie = request()->cookie('default_client', '');
        if ($cookie != "") {
            return AjaxReturn(1);
        }
    }

    /**
     * 首页领用优惠券
     */
    public function getCoupon()
    {
        if (! empty($this->uid)) {
            $coupon_type_id = request()->post('coupon_type_id', 0);
            $member = new Member();
            $retval = $member->memberGetCoupon($this->uid, $coupon_type_id, 2);
            return AjaxReturn($retval);
        } else {
            return AjaxReturn(NO_LOGIN);
        }
    }

    /**
     * 查看首页商城热卖更多
     */
    public function getGoodsHotList()
    {
        if (request()->isAjax()) {
            $goods_platform = new Platform();
            $condition['is_hot'] = 1;
            $val['nss.shop_state'] = 1;
            $goods_hot_list = $goods_platform->getPlatformGoodsList(1, 0, $condition);
            return $goods_hot_list;
        }
        $this->style = "wap/aozhou/";
        $style = "wap/aozhou/";
        $this->assign('style', $style);
        return view($this->style . 'Index/hot');
    }

    /**
     * 查看首页商城推荐更多  只用于澳洲模板 2017-10-10
     */
    public function getGoodsRecommendList()
    {
        if (request()->isAjax()) {
            $goods_platform = new Platform();
            $condition['is_recommend'] = 1;
            $val['nss.shop_state'] = 1;
            $goods_recommend_list = $goods_platform->getPlatformGoodsList(1, 0, $condition);
            return $goods_recommend_list;
        }
        $this->style = "wap/aozhou/";
        $style = "wap/aozhou/";
        $this->assign('style', $style);
        return view($this->style . 'Index/recommend');
    }
    /**
     * 店铺街
     */
    public function shopStreet()
    {
        $shop = new Shop();
        $shop_name = request()->get("shop_name",""); // 店铺名称
        $shop_group_id = request()->get("shop_group_id","");// 店铺分类
        $shop_group_name = request()->get("shop_group_name","");// 店铺名称
        $order_type = request()->get("order_type","");// 排序类型 为1销售排行2信誉排行
        $sort = request()->get("sort",""); // 倒排正排
        
        $order = "ns.shop_sort " . $sort;
        if ($order_type == 1) {
            $order = "ns.shop_sales " . $sort;
        } else
            if ($order_type == 2) {
                $order = "ns.shop_credit " . $sort;
            }
    
        $condition['ns.shop_state'] = 1;
        if (! empty($shop_group_id)) {
            $condition['ns.shop_group_id'] = $shop_group_id;
        }
    
        if (! empty($shop_name)) {
            $condition['ns.shop_name'] = array(
                "like",
                "%" . $shop_name . "%"
            );
        }
    
        $shop_list = $shop->getShopList(1, 0, $condition, $order); // 店铺查询
        $shop_group_list = $shop->getShopGroup(); // 店铺分类
        $assign_get_list = array(
            'order_type' => $order_type, // 排序类型
            'shop_group_id' => $shop_group_id, // 店铺类型
            'shop_name' => $shop_name, // 搜索名称
            'sort' => $sort, // 排序
            'shop_list' => $shop_list['data'], // 店铺列表
            'total_count' => $shop_list['total_count'], // 总条数
            'shop_group_list' => $shop_group_list['data']
        ); // 店铺分页
    
        foreach ($assign_get_list as $key => $value) {
            $this->assign($key, $value);
        }
        $this->assign('shop_group_name', $shop_group_name);
        return view($this->style . '/Index/shopStreet');
    }
    
    /**
     * 获取推荐商品 新品 精品 热卖
     */
    public function getAdminRecommendGoodsList(){
        $recommend_goods_list = array();
        $recommend_goods_list['goods_new_list'] = Cache::tag("wap_goodslist")->get("wap_goods_new_list");
        $recommend_goods_list['goods_recommend_list'] = Cache::tag("wap_goodslist")->get("wap_goods_recommend_list");
        $recommend_goods_list['goods_hot_list'] = Cache::tag("wap_goodslist")->get("wap_goods_hot_list");
    
        $goods_service = new Goods();
        $page_size = 4;
        $goods_field = "ng.goods_id,ng.goods_name,ng_sap.pic_cover_mid,ng.promotion_price,ng.stock,ng.sales,ng.point_exchange,ng.point_exchange_type,ng.shipping_fee";
        $condition = array(
            "ng.state" => 1,
        );
        if(empty($recommend_goods_list['goods_new_list'])){
            $condition["ng.is_new"] = 1;
            $goods_new_list = $goods_service -> getGoodsQueryLimit($condition, $goods_field, $page_size);
            $recommend_goods_list['goods_new_list'] = $goods_new_list;
            Cache::tag("wap_goodslist")->set("wap_goods_new_list", $goods_new_list);
        }
        if(empty($recommend_goods_list['goods_recommend_list'])){
            $condition["ng.is_recommend"] = 1;
            $goods_recommend_list = $goods_service -> getGoodsQueryLimit($condition, $goods_field, $page_size);
            $recommend_goods_list['goods_recommend_list'] = $goods_recommend_list;
            Cache::tag("wap_goodslist")->set("wap_goods_recommend_list", $goods_recommend_list);
        }
        if(empty($recommend_goods_list['goods_hot_list'])){
            $condition["ng.is_hot"] = 1;
            $goods_hot_list = $goods_service -> getGoodsQueryLimit($condition, $goods_field, $page_size);
            $recommend_goods_list['goods_hot_list'] = $goods_hot_list;
            Cache::tag("wap_goodslist")->set("wap_goods_hot_list", $goods_hot_list);
        }
        return $recommend_goods_list;
    }
    
    /**
     * 公告详情
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function noticeContent()
    {
        $notice_id = request()->get('id', '');
        $goods_platform = new Platform();
        $notice_info = $goods_platform->getNoticeDetail($notice_id);
        if (empty($notice_info)) {
            $this->error("未获取到公告信息");
        }
        $this->assign('article_info', $notice_info);
    
        // 上一篇
        $prev_info = $goods_platform->getNoticeList(1, 1, [
            "id" => array(
                "<",
                $notice_id
            )
        ], "id desc");
        $this->assign("prev_info", $prev_info['data'][0]);
        // 下一篇
        $next_info = $goods_platform->getNoticeList(1, 1, [
            "id" => array(
                ">",
                $notice_id
            )
        ], "id asc");
        $this->assign("next_info", $next_info['data'][0]);
        return view($this->style . 'Index/noticeContent');
    }
    
    /**
     * 公告列表
     */
    public function noticeList()
    {
        if (request()->isAjax()) {
            $page = request()->post("page", 1);
            $goods_platform = new Platform();
            $article_list = $goods_platform->getNoticeList($page, 0, '', 'sort desc');
            return $article_list;
        }
        return view($this->style . 'Index/noticeList');
    }


}