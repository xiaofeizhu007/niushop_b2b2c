<?php
/**
 * Shop.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\wap\controller;

use data\service\NfxUser;
use data\service\Goods as GoodsService;
use data\service\Member as MemberService;
use data\service\Pintuan;
use data\service\Promotion;
use data\service\Shop as ShopService;
use data\service\Weixin;
use data\service\Config;
use data\service\Platform;

/**
 * 店铺控制器
 *
 * @author Administrator
 *        
 */
class Shop extends BaseController
{

    /**
     * 店铺主页
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    
    /**
     * 店铺主页
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function index()
    {   
        $this->assign("is_subscribe", 0);
        $shop = new ShopService();
        $shop_id = request()->get('shop_id', '');
        $shop_info = $shop->getShopInfo($shop_id);
        $member = new MemberService();
        $goods = new GoodsService();
        $source_user_name = "";
        $shop_ad_list = $shop->getShopAdList(1, 0, [
            'type' => 1,"shop_id" => $shop_id
        ]);
        $this->assign('shop_id', $shop_id);
        $this->assign('source_user_name', $source_user_name);
        $this->assign('shop_ad_list', $shop_ad_list["data"]);
        if (null == $shop_info) {
            $this->redirect(__URL(__URL__.'/wap')); // 没有商品返回到首页
        }
        $this->assign("shop_info", $shop_info);
        $config = new Config();
        $notice_arr = $config->getNotice($shop_id);
        $this->assign('notice', $notice_arr);
        // 是否收藏店铺
        $is_member_fav_shop = $member->getIsMemberFavorites($this->uid, $shop_id, 'shop');
        $this->assign("is_member_fav_shop", $is_member_fav_shop);
//         // 新品推荐,推荐专区,热销专区
        $title_arr = [
            "新品推荐",
            "推荐专区",
            "热销专区"
        ];
        $conditions = array(
            [
                'ng.shop_id' => $shop_id,
                'is_new' => 1
            ],
            [
                'ng.shop_id' => $shop_id,
                'is_recommend' => 1
            ],
            [
                'ng.shop_id' => $shop_id,
                'is_hot' => 1
            ]
        );
     
        $Platform = new Platform();
        $recommend_block = $Platform->getshopPlatformGoodsRecommendClass($shop_id);
        $fav_count = $shop->getShopFavoritesNum($shop_id);
        //         foreach($recommend_block as $k=>$v){
//             //获取模块下商品
//             $goods_list = $Platform->getPlatformGoodsRecommend($v['class_id']);
//             if(empty($goods_list)){
//                 unset($recommend_block[$k]);
//             }
//         }

//         $goods_list = array();
//         foreach ($conditions as $key => $item) {

//             $temp_list = $goods->getGoodsList(1, 14, $item, 'ng.sort');
//             $goods_list[$key]["goods_list"] = $temp_list["data"];
//             $goods_list[$key]["count"] = count($temp_list["data"]);
//             $goods_list[$key]["title"] = $title_arr[$key];
//         }
        $this->assign("recommend_block", $recommend_block);
        
        $ticket = $this->getShareTicket();
        $this->assign('is_shop_member', 1);
        $this->assign("signPackage", $ticket);
     //   $this->assign("recommend_block", $recommend_block);
        $is_subscribe = 0; // 标识：是否显示顶部关注 0：[隐藏]，1：[显示]
                           // 检查是否配置过微信公众号
        $this->assign("is_subscribe", $is_subscribe);
        $this->assign("fav_count", $fav_count);

        
        // 店铺首页优惠券
        $coupon_list = $member->getMemberCouponTypeList($shop_id, $this->uid);
//         var_dump($coupon_list);
        $this->assign('coupon_list', $coupon_list);
        
        
        // 限时折扣列表
        $condition['status'] = 1;
        $condition['ng.state'] = 1;
        $condition['shop_id'] = $shop_id;
        $discount_list = $goods->getDiscountGoodsList(1, 0, $condition, 'end_time');
        
        foreach ($discount_list['data'] as $k => $v) {
            $v['discount'] = str_replace('.00', '', $v['discount']);
            // $v['promotion_price'] = str_replace('.00', '', $v['promotion_price']);
            // $v['price'] = str_replace('.00', '', $v['price']);
        }
        
        $this->assign('discount_list', $discount_list['data']);
        return view($this->style . '/Shop/index');
    }

    /**
     * 获取推荐类型商品列表
     */
    public function goodsList()
    {
        $shop_id = request()->get("shop_id", 0);
        $type = request()->get("type", 0);
        $this->assign('type',$type);
        $this->assign('shop_id',$shop_id);
        switch ($type) {
            case 1:
                $title = '新品商品';
                break;
            case 2:
                $title = '热销商品';
                break;
            case 3:
                $title = '推荐商品';
                break;
            default:
                $title = '全部商品';
                break;
        }
        $this->assign("search_title", $title);
        if(request()->isAjax()){
            $type = request()->post('type',0);
            $shop_id = request()->post('shop_id',0);
            $components = new Components();
            $goods_list = $components->getTypeGoodsList($shop_id, $type,0);
            return ajaxReturn(1,$goods_list);
        }

        return view($this->style . '/Shop/goodsList');
    }

    /**
     * 关注店铺
     */
    public function userAssociateShop()
    {
        if (empty($this->uid)) {
            return - 1;
        } else {
            $nfx_user = new NfxUser();
            $shop_id = isset($_POST['shop_id']) ? $_POST['shop_id'] : '';
            $session_id = 0;
            if (! empty($_SESSION["source_shop_id"])) {
                if ($shop_id == $_SESSION["source_shop_id"]) {
                    $session_id = $_SESSION["source_uid"];
                }
            }
            $retval = $nfx_user->userAssociateShop($this->uid, $shop_id, $session_id);
            return AjaxReturn($retval);
        }
    }
    
    /**
     * 检测用户
     */
    private function checkLogin()
    {
        $uid = $this->uid;
        if (empty($uid)) {
            $redirect = __URL(__URL__ . "/wap/login");
            $this->redirect($redirect); // 用户未登录
        }
        $is_member = $this->user->getSessionUserIsMember();
        if (empty($is_member)) {
            $redirect = __URL(__URL__ . "/wap/login");
            $this->redirect($redirect); // 用户未登录
        }
    }
    /**
     * 申请店铺
     */
    public function applyShop(){
        $this->checkLogin();
        
        $shop = new ShopService();
        if (request()->isAjax()) {
            $apply_type = isset($_POST['apply_type']) ? $_POST['apply_type'] : '';
            $uid = $this->user->getSessionUid();
            $company_name = isset($_POST['company_name']) ? $_POST['company_name'] : '';
            $company_province_id = isset($_POST['company_province_id']) ? $_POST['company_province_id'] : '';
            $company_city_id = isset($_POST['company_city_id']) ? $_POST['company_city_id'] : '';
            $company_district_id = isset($_POST['company_district_id']) ? $_POST['company_district_id'] : '';
            $company_address_detail = isset($_POST['company_address_detail']) ? $_POST['company_address_detail'] : '';
            $company_phone = isset($_POST['company_phone']) ? $_POST['company_phone'] : '';
            $company_type = isset($_POST['company_type']) ? $_POST['company_type'] : 1;
            $company_employee_count = isset($_POST['company_employee_count']) ? $_POST['company_employee_count'] : 1;
            $company_registered_capital = isset($_POST['company_registered_capital']) ? $_POST['company_registered_capital'] : 0;
            $contacts_name = isset($_POST['contacts_name']) ? $_POST['contacts_name'] : '';
            $contacts_phone = isset($_POST['contacts_phone']) ? $_POST['contacts_phone'] : '';
            $contacts_email = isset($_POST['contacts_email']) ? $_POST['contacts_email'] : '';
            $contacts_card_no = isset($_POST['contacts_card_no']) ? $_POST['contacts_card_no'] : '';
            $contacts_card_electronic_1 = isset($_POST['contacts_card_electronic_1']) ? $_POST['contacts_card_electronic_1'] : '';
            $contacts_card_electronic_2 = isset($_POST['contacts_card_electronic_2']) ? $_POST['contacts_card_electronic_2'] : '';
            $contacts_card_electronic_3 = isset($_POST['contacts_card_electronic_3']) ? $_POST['contacts_card_electronic_3'] : '';
            $business_licence_number = isset($_POST['business_licence_number']) ? $_POST['business_licence_number'] : '';
            $business_sphere = isset($_POST['business_sphere']) ? $_POST['business_sphere'] : '';
            $business_licence_number_electronic = isset($_POST['business_licence_number_electronic']) ? $_POST['business_licence_number_electronic'] : '';
            $organization_code = isset($_POST['organization_code']) ? $_POST['organization_code'] : '';
            $organization_code_electronic = isset($_POST['organization_code_electronic']) ? $_POST['organization_code_electronic'] : '';
            $general_taxpayer = isset($_POST['general_taxpayer']) ? $_POST['general_taxpayer'] : '';
            $bank_account_name = isset($_POST['bank_account_name']) ? $_POST['bank_account_name'] : '';
            $bank_account_number = isset($_POST['bank_account_number']) ? $_POST['bank_account_number'] : '';
            $bank_name = isset($_POST['bank_name']) ? $_POST['bank_name'] : '';
            $bank_code = isset($_POST['bank_code']) ? $_POST['bank_code'] : '';
            $bank_address = isset($_POST['bank_address']) ? $_POST['bank_address'] : '';
            $bank_licence_electronic = isset($_POST['bank_licence_electronic']) ? $_POST['bank_licence_electronic'] : '';
            $is_settlement_account = isset($_POST['is_settlement_account']) ? $_POST['is_settlement_account'] : 1;
            $settlement_bank_account_name = isset($_POST['settlement_bank_account_name']) ? $_POST['settlement_bank_account_name'] : '';
            $settlement_bank_account_number = isset($_POST['settlement_bank_account_number']) ? $_POST['settlement_bank_account_number'] : '';
            $settlement_bank_name = isset($_POST['settlement_bank_name']) ? $_POST['settlement_bank_name'] : '';
            $settlement_bank_code = isset($_POST['settlement_bank_code']) ? $_POST['settlement_bank_code'] : '';
            $settlement_bank_address = isset($_POST['settlement_bank_address']) ? $_POST['settlement_bank_address'] : '';
            $tax_registration_certificate = isset($_POST['tax_registration_certificate']) ? $_POST['tax_registration_certificate'] : '';
            $taxpayer_id = isset($_POST['taxpayer_id']) ? $_POST['taxpayer_id'] : '';
            $tax_registration_certificate_electronic = isset($_POST['tax_registration_certificate_electronic']) ? $_POST['tax_registration_certificate_electronic'] : '';
            $shop_name = isset($_POST['shop_name']) ? $_POST['shop_name'] : '';
            $apply_state = isset($_POST['apply_state']) ? $_POST['apply_state'] : 1;
            $apply_message = isset($_POST['apply_message']) ? $_POST['apply_message'] : '';
            $apply_year = isset($_POST['apply_year']) ? $_POST['apply_year'] : 1;
            $shop_type_name = isset($_POST['shop_type_name']) ? $_POST['shop_type_name'] : '';
            $shop_type_id = isset($_POST['shop_type_id']) ? $_POST['shop_type_id'] : 0;
            $shop_group_name = isset($_POST['shop_group_name']) ? $_POST['shop_group_name'] : '';
            $shop_group_id = isset($_POST['shop_group_id']) ? $_POST['shop_group_id'] : 0;
            $paying_money_certificate = isset($_POST['paying_money_certificate']) ? $_POST['paying_money_certificate'] : '';
            $paying_money_certificate_explain = isset($_POST['paying_money_certificate_explain']) ? $_POST['paying_money_certificate_explain'] : '';
            $paying_amount = isset($_POST['paying_amount']) ? $_POST['paying_amount'] : 0;
            $recommend_uid = isset($_POST["recommend_uid"]) ? $_POST["recommend_uid"] : 0;
            
            $bank_type = request()->post("bank_type", 1);
            $bank_real_name = request()->post("bank_real_name", '');
            $bank_mobile = request()->post("bank_mobile", '');
            $retval = $shop->addShopApply($apply_type, $uid, $company_name, $company_province_id, $company_city_id, $company_district_id, $company_address_detail, $company_phone, $company_type, $company_employee_count, $company_registered_capital, $contacts_name, $contacts_phone, $contacts_email, $contacts_card_no, $contacts_card_electronic_1, $contacts_card_electronic_2, $contacts_card_electronic_3, $business_licence_number, $business_sphere, $business_licence_number_electronic, $organization_code, $organization_code_electronic, $general_taxpayer, $bank_account_name, $bank_account_number, $bank_name, $bank_code, $bank_address, $bank_licence_electronic, $is_settlement_account, $settlement_bank_account_name, $settlement_bank_account_number, $settlement_bank_name, $settlement_bank_code, $settlement_bank_address, $tax_registration_certificate, $taxpayer_id, $tax_registration_certificate_electronic, $shop_name, $apply_state, $apply_message, $apply_year, $shop_type_name, $shop_type_id, $shop_group_name, $shop_group_id, $paying_money_certificate, $paying_money_certificate_explain, $paying_amount, $recommend_uid, $bank_type, $bank_real_name, $bank_mobile);
            return AjaxReturn($retval);
        } else {
            $where = array(
                'is_self_support' => 2,
                'is_open_level' => 1
            );
            $shop_type_list = $shop->getShopTypeList(1, 0, $where, '');
            $this->assign('shop_type_list', $shop_type_list['data']);
        
            $shop_group = $shop->getShopGroup();
            $this->assign('shop_group', $shop_group['data']);
        
            $apply_state = $this->user->getMemberIsApplyShop($this->uid);
            $this->assign("apply_state", $apply_state);
        
            return view($this->style . 'Shop/applyShop');
        }
    }

    /**
     * 店铺详情
     */
    public function shopDetail()
    {
        $shop = new ShopService();
        $shop_id = request()->get('shop_id', '');
        if($shop_id== ''){
            $this->error('店铺不存在');
        }
        $shop_info = $shop->getShopDetail($shop_id);
        if(!$shop_info['base_info']){
            $this->error('店铺不存在');
        }
//        dump($shop_info);
        $member = new MemberService();
        $goods = new GoodsService();
        $source_user_name = "";
        $this->assign('shop_id', $shop_id);
        $this->assign('source_user_name', $source_user_name);
        $this->assign("shop_info", $shop_info['base_info']);
        $this->assign("group_info", $shop_info['group_info']);
        $this->assign("type_info", $shop_info['instance_type_info']);

        $config = new Config();
        $notice_arr = $config->getNotice($shop_id);
        $this->assign('notice', $notice_arr);
        // 是否收藏店铺
        $is_member_fav_shop = $member->getIsMemberFavorites($this->uid, $shop_id, 'shop');
        $this->assign("is_member_fav_shop", $is_member_fav_shop);

        $fav_count = $shop->getShopFavoritesNum($shop_id);

        $ticket = $this->getShareTicket();
        $this->assign('is_shop_member', 1);
        $this->assign("signPackage", $ticket);
        //   $this->assign("recommend_block", $recommend_block);
        $is_subscribe = 0; // 标识：是否显示顶部关注 0：[隐藏]，1：[显示]
        // 检查是否配置过微信公众号
        $this->assign("is_subscribe", $is_subscribe);
        $this->assign("fav_count", $fav_count);
        return view($this->style . '/Shop/detail');
    }

    /**
     * 店铺活动页
     */
    public function shopPromotion()
    {
        $shop_id = request()->get('shop_id','');
        if($shop_id == ''){
            $this->error('店铺不存在');
        }
        $shop = new ShopService();
        $shop_info = $shop->getShopDetail($shop_id);
        if(!$shop_info['base_info']){
            $this->error('店铺不存在');
        }
        // 首页轮播图
        $ad_condition = [
            'shop_id' => $shop_id,
            'type' => 1,
        ];
        $shop_adv_list = $shop->getShopAdList(1,0,$ad_condition,'sort asc');
        $this->assign('shop_adv_list', $shop_adv_list);
        // 限时折扣列表
        $goods = new GoodsService();
        $condition['status'] = 1;
        $condition['ng.state'] = 1;
        $condition['shop_id'] = $shop_id;
        $discount_list = $goods->getDiscountGoodsList(1, 0, $condition, 'end_time');
//        dump($discount_list);
        foreach ($discount_list['data'] as $k => $v) {
            $v['discount'] = str_replace('.00', '', $v['discount']);
            // $v['promotion_price'] = str_replace('.00', '', $v['promotion_price']);
            // $v['price'] = str_replace('.00', '', $v['price']);
        }

        $this->assign('discount_list', $discount_list['data']);
        // 获取当前时间
        $current_time = $this->getCurrentTime();
        $this->assign('ms_time', $current_time);

        // 首页优惠券
        $member = new MemberService();
        $coupon_list = $member->getMemberCouponTypeList($shop_id, $this->uid);
        $this->assign('coupon_list', $coupon_list);

        // 游戏活动
        $promotion = new Promotion();
        $gameList = $promotion->getPromotionGamesList(1, 0, [
            'status' => 1,
            'shop_id' => $shop_id,
            "activity_images" => [
                "neq",
                ""
            ]
        ], 'game_id desc');

        $this->assign("gameList", $gameList);
        $this->assign("shop_id", $shop_id);
        return view($this->style . 'Shop/shopPromotion');
    }


    /**
     * 首页领用优惠券
     */
    public function getCoupon()
    {
        if (! empty($this->uid)) {
            $coupon_type_id = request()->post('coupon_type_id', 0);
            $member = new Member();
            $retval = $member->memberGetCoupon($this->uid, $coupon_type_id, 2);
            return AjaxReturn($retval);
        } else {
            return AjaxReturn(NO_LOGIN);
        }
    }

    /**
     * 得到当前时间戳的毫秒数
     *
     * @return number
     */
    public function getCurrentTime()
    {
        $time = time();
        $time = $time * 1000;
        return $time;
    }

}
