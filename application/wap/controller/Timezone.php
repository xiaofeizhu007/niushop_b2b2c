<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/3/6 0006
 * Time: 下午 2:36
 */
namespace app\wap\controller;

use think\Session;
use think\Controller;

class Timezone extends Controller
{
    public function getClientTimezone()
    {
        if(request()->isAjax()){
            $clientTimezone = request()->post('clientTimezone', '');
            $isDst = request()->post('isDst', false);
            if(!empty($clientTimezone)){
                Session::set('clientTimezone', $clientTimezone);
                Session::set('isDst', $isDst);
                return 1;
            }
        }
    }
}