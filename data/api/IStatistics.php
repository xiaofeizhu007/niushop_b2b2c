<?php
/**
 * IGoodsGroup.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace data\api;
/**
 * 统计服务层api
 */
interface IStatistics
{
    /**
     * 统计会员数量
     * @param unknown $condition
     */
    function getMemberCount($condition);
    /**
     * 统计总积分数
     */
    function getIntegralSum($condition);
    /**
     * 统计发放积分数
     * @param unknown $condition
     */
    function getGrantIntegralSum($condition);
    /**
     * 统计用户消费
     * @param unknown $condition
     */
    function getMemberUseMoney($condition, $order_condition);
    /**
     * 统计用户产生订单
     * @param unknown $condition
     */
    function getMemberUseNum($condition, $order_condition);
    /**
     * 销售量
     * @param unknown $condition
     */
    function getOrderCount($condition);
    /**
     * 销售额
     * @param unknown $condition
     */
    function getOrderMoney($condition);
    /**
     * 销售商品数
     * @param unknown $condition
     */
    function getSaleGoodsCount($condition);
    /**
     * 查看店铺数量
     * @param unknown $condition
     */
    function getShopCount($condition);
    /**
     * 创建商品数
     */
    function getGoodsCount($condition);
    /*
     * 获取平台订单总额对应明细
     */
    public function getPlatformAccountOrderRecordList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '');
    /*
     * 获取营业总额对应明细
     */
    public function getPlatformAccountTotalRecordList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '');
}