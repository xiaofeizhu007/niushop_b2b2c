<?php
namespace data\extend\aliyun\samples;

if (is_file(__DIR__ . '/../autoload.php')) {
    require_once __DIR__ . '/../autoload.php';
}
use OSS\OssClient;
use OSS\Core\OssException;
use data\service\Config;

/**
 * 
 */
class Aliyun{
    
    // accessKeyId
    private $accessKeyId;
    
    // accessKeySecret
    private $accessKeySecret;
    
    // Endpoint
    private $endpoint;
    
    // 外网访问域名
    private $out_url;
    
    // cdn加速访问域名
    private $cdn_url;
    
    // 存储空间名称
    private $bucket;
    
    // 实例化对象
    private $ossClient;
    
    public function __construct()
    {
        $this->init();
    }
    
    /**
     * 初始化
     */
    private function init(){
        
        $config_service = new Config();
        $config = $config_service->getAliyunConfig(0);
        
        $this->accessKeyId = $config['accessKeyId'];
        $this->accessKeySecret = $config['accessKeySecret'];
        $this->endpoint = $config['endpoint'];
        $this->bucket = $config['bucket'];
        $this->out_url = $config['out_url'];
        $this->cdn_url = $config['cdn_url'];
        
        $this->ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
    }
    
    /**
     * 成功的提示
     */
    private function success($path = '', $message = ''){
        
        return [
            'code' => 1,
            'path' => $path,
            'message' => $message,
        ];
    }
    
    /**
     * 失败的提示
     */
    private function error($path = '', $message = ''){
        
        return [
            'code' => -1,
            'path' => $path,
            'message' => $message,
        ];
    }
    
    /**
     * 功能说明：上传文件
     * 参数说明：
     * $object string 要上传的文件名称 
     * $filePath string 上传文件本地路径
     */
    public function uploadFile($object, $filePath){
        
        try{
            
            $res = $this->ossClient->uploadFile($this->bucket, $object, $filePath);
            
            //三种情况 使用返回文件路径访问 使用外网域名访问 使用cdn域名访问
            if($this->cdn_url == ''){
                if($this->out_url == '') $url_type = 0;
                else $url_type = 1;
            }else{
                $url_type = 2;
            }
            
            if($url_type == 0) $url = $res['info']['url'];
            if($url_type == 1) $url = $this->out_url . '/' . $object;
            if($url_type == 2) $url = $this->cdn_url . '/' . $object;
            
            return $this->success($url, 'success');
            
        } catch(OssException $e) {
            
            return $this->error($e->getMessage(), 'error');
        }
        
    }
    
    /**
     * 功能说明：删除文件
     * 参数说明：
     * $object string 要删除的文件名称
     */
    public function deleteFile($object){
        
        try{
            
            $pos = strpos($object, '/', 7);
            $file = substr($object, strpos($object, '/', 7) + 1);
            
            $res = $this->ossClient->deleteObject($this->bucket, $file);
        
            return $this->success($res, 'success');
        
        } catch(OssException $e) {
        
            return $this->error($e->getMessage(), 'error');
        }
    }
}
