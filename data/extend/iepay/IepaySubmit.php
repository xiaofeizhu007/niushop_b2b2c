<?php
namespace data\extend\iepay;
/* *
 * 类名：IepaySubmit
 * 功能：iepay各接口请求提交类
 * 详细：构造iepay各接口，获取远程HTTP数据
 */
require_once("iepay_core.function.php");
require_once("iepay_md5.function.php");

class IepaySubmit {

    public $iepay_config;
    /**
     *iepay create order
     */
    public $iepay_gateway_create = 'https://a.mypaynz.com/api/online';
    /**
     * iepay check order
     * */
    public $iepay_gateway_check = 'https://a.mypaynz.com/api/check_order_status';
    /**
     * iepay refund order
     * */
    public $iepay_gateway_refund = 'https://a.mypaynz.com/api/refund';

    function __construct($iepay_config){
        $this->iepay_config = $iepay_config;
    }
    function IepaySubmit($iepay_config) {
        $this->__construct($iepay_config);
    }

    /**
     * 生成签名结果
     * @param $para_sort 已排序要签名的数组
     * return 签名结果字符串
     */
    function buildRequestMysign($para_sort) {

        //把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        $prestr = createLinkstring($para_sort);

        $sign = md5Sign($prestr, $this->iepay_config['apikey']);

        return $sign;
    }

    /**
     * 生成要请求给iepay的参数数组
     * @param $para_temp 请求前的参数数组
     * @return 要请求的参数数组
     */
    function buildRequestPara($para_temp) {
        //除去待签名参数数组中的空值和签名参数
//        $para_filter = paraFilter($para_temp);

        //对待签名参数数组排序
        $para_sort = argSort($para_temp);

        //生成签名结果
        $sign = $this->buildRequestMysign($para_sort);

        $para_temp['sign'] = $sign;

        return $para_temp;
    }

    /**
     * 建立请求，以模拟远程HTTP的POST请求方式构造并获取iepay的处理结果
     * @param $para_temp 请求参数数组
     * @param $iepay_type create,check,refund
     * @return iepay处理结果
     */
    function buildRequestHttp($para_temp,$iepay_type) {

        $result = '';
        //待请求参数数组字符串
        $request_data = $this->buildRequestPara($para_temp);

        if($iepay_type =="create"){
            $result = getHttpResponsePOST($this->iepay_gateway_create, $request_data,trim(strtolower($this->iepay_config['input_charset'])));
        }else if($iepay_type =="check"){
            $result = getHttpResponsePOST($this->iepay_gateway_check, $request_data,trim(strtolower($this->iepay_config['input_charset'])));
        }else if($iepay_type =="refund"){
            $result = getHttpResponsePOST($this->iepay_gateway_refund, $request_data,trim(strtolower($this->iepay_config['input_charset'])));
        }else{
            $result = '';
        }

        return $result;
    }
}
?>