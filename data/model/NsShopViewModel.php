<?php

/**
 * NcCmsCommentViewModel.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace data\model;

use data\model\BaseModel as BaseModel;

class NsShopViewModel extends BaseModel
{
    protected $table = 'ns_shop';
    /**
     * 获取列表返回数据格式
     * @param unknown $page_index
     * @param unknown $page_size
     * @param unknown $condition
     * @param unknown $order
     * @return unknown
     */
    public function getViewList($page_index, $page_size, $condition, $order){
    
        $queryList = $this->getViewQuery($page_index, $page_size, $condition, $order);
        $queryCount = $this->getViewCount($condition);
        $list = $this->setReturnList($queryList, $queryCount, $page_size);
        return $list;
    }
    /**
     * 获取列表
     * @param unknown $page_index
     * @param unknown $page_size
     * @param unknown $condition
     * @param unknown $order
     * @return \data\model\multitype:number
     */
    public function getViewQuery($page_index, $page_size, $condition, $order)
    {
        //设置查询视图
        $viewObj = $this->alias('ns')
        ->join('ns_shop_group nsg','ns.shop_group_id = nsg.shop_group_id','left')
        ->join('sys_instance_type sit','`ns`.shop_type = sit.instance_typeid','left')
        ->join('sys_user su', 'ns.uid = su.uid', 'left')
        ->field('ns.shop_id, ns.shop_name, ns.shop_type, ns.uid, ns.shop_group_id, ns.shop_company_name, ns.province_id, ns.city_id, ns.shop_address, ns.shop_zip, ns.shop_state, ns.shop_close_info, ns.shop_sort, ns.shop_logo, ns.shop_banner, ns.shop_avatar, ns.shop_keywords, ns.shop_description, ns.shop_qq, ns.shop_ww, ns.shop_phone, ns.shop_domain, ns.shop_domain_times, ns.shop_recommend, ns.shop_desccredit, ns.shop_credit, ns.shop_servicecredit, ns.shop_deliverycredit, ns.shop_collect, ns.shop_stamp, ns.shop_printdesc, ns.shop_sales, ns.shop_account, ns.shop_cash, ns.shop_workingtime, ns.live_store_name, ns.live_store_address, ns.live_store_tel, ns.shop_vrcode_prefix, ns.live_store_bus, ns.store_qtian, ns.shop_zhping, ns.shop_erxiaoshi, ns.shop_tuihuo, ns.shop_shiyong, ns.shop_shiti, ns.shop_xiaoxie, ns.shop_huodaofk, ns.shop_free_time, ns.shop_region, ns.recommend_uid, ns.shop_qrcode, ns.shop_create_time, ns.shop_end_time, ns.shop_platform_commission_rate, ns.latitude_longitude, ns.presales, ns.aftersales, ns.workingtime, ns.shop_bg, ns.shop_notice, nsg.group_name, sit.type_name as shop_type_name, su.user_name AS username ');
        $list = $this->viewPageQuery($viewObj, $page_index, $page_size, $condition, $order);
        return $list;
    }
    /**
     * 获取列表数量
     * @param unknown $condition
     * @return \data\model\unknown
     */
    public function getViewCount($condition)
    {
        $viewObj = $this->alias('ns')
        ->join('ns_shop_group nsg','ns.shop_group_id = nsg.shop_group_id','left')
        ->join('sys_instance_type sit','`ns`.shop_type = sit.instance_typeid','left')
        ->join('sys_user su', 'ns.uid = su.uid', 'left')
        ->field('ns.shop_id');
        $count = $this->viewCount($viewObj,$condition);
        return $count;
    }
}

?>