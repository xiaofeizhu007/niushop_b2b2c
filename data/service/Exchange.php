<?php
/**
 * AdminUser.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.4.24
 * @version : v1.0.0.0
 */
namespace data\service;
use data\api\IExchange;
use data\service\BaseService as BaseService;
use data\model\NsGoodsModel;
use data\model\NsOrderModel;
use data\service\Order\OrderExchange;
use data\service\Order\OrderBusiness;
use data\service\Order;
class Exchange extends Order implements IExchange
{

    function __construct()
    {
        parent::__construct();
    }
	/* (non-PHPdoc)
     * @see \data\api\IExchange::modifyGoodsPointExchangeType()
     * $point_exchange_type 积分兑换类型 0 非积分兑换 1 只能积分兑换 
     */
    public function modifyGoodsPointExchangeType($goods_id, $point_exchange_type, $point_exchange)
    {
        // TODO Auto-generated method stub
        $goods_model = new NsGoodsModel();
        $data = array(
            "point_exchange_type" => $point_exchange_type,
            "point_exchange" => $point_exchange
        );
        $retval = $goods_model->save($data, ["goods_id" => $goods_id]);
        return $retval;
    }
    /**
     * 积分兑换订单生成
     */
    public function exchangeOrderCreate($order_type, $out_trade_no, $pay_type, $shipping_type, $order_from, $buyer_ip, $buyer_message, $buyer_invoice, $shipping_time, $receiver_mobile, $receiver_province, $receiver_city, $receiver_district, $receiver_address, $receiver_zip, $receiver_name, $point, $user_money, $goods_sku_list, $platform_money, $pick_up_id, $shipping_company_id, $coin, $id_card = ''){
        
        $order_exchange = new OrderExchange();
 
        $retval = $order_exchange->orderCreateExchange($order_type, $out_trade_no, $pay_type, $shipping_type, $order_from, $buyer_ip, $buyer_message, $buyer_invoice, $shipping_time, $receiver_mobile, $receiver_province, $receiver_city, $receiver_district, $receiver_address, $receiver_zip, $receiver_name, $point, $user_money, $goods_sku_list, $platform_money, $pick_up_id, $shipping_company_id, $coin, '', $id_card);
        runhook("Notify", "orderCreate", array(
        "order_id" => $retval
        ));
        // 针对特殊订单执行支付处理
        if ($retval > 0) {
            hook('orderCreateSuccess', [
            'order_id' => $retval
            ]);
            $order_model = new NsOrderModel();
            $order_info = $order_model->getInfo([
                'order_id' => $retval
            ], '*');
            if (! empty($order_info)) {
                if ($order_info['user_platform_money'] != 0) {
                    if ($order_info['pay_money'] == 0) {
                        $this->orderOnLinePay($out_trade_no, 5,"");
                    }
                } else {
        
                    if ($order_info['pay_money'] == 0) {
                        $this->orderOnLinePay($out_trade_no, 1,""); // 默认微信支付
                    }
                }
            }
        }
        
        return $retval;
    }
    
    


    
}