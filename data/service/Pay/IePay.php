<?php
/**
 * AliPay.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace data\service\Pay;

use data\service\Pay\PayParam;
use data\extend\iepay\IepaySubmit as IepaySubmit;
use think\Cookie;
use think\Request;
use think\Log;

/**
 * 功能说明：自定义IEPAY支付接入类
 */
class IePay extends PayParam
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // 防止默认目录错误
    }

    /**
     * 基本设置
     *
     * @return unknown
     */
    public function getIepayConfig()
    {
        // mid
        $iepay_config['mid'] = $this->mid;
        
        // apikey
        $iepay_config['apikey'] = $this->apikey;

        // 字符编码格式 目前支持 gbk 或 utf-8
        $iepay_config['input_charset'] = strtolower('utf-8');
        return $iepay_config;
    }
    /**
     * iepay的pay_type的转换
     * */
    public function changePay_type($pay_type)
    {
        switch ($pay_type)
        {
            case 'qrcode_alipay':
                return 'IE0011';
            case 'web_alipay':
                return 'IE0012';
            case 'wap_alipay':
                return 'IE0013';
            case 'app_alipay':
                return 'IE0015';
            case 'qrcode_wechat':
                return 'IE0021';
            case 'wap_wechat':
                return 'IE0022';
            case 'wap_wechat_quickpay':
                return 'IE0023';
            case 'app_wechat':
                return 'IE0025';
            case 'UNION':
                return 'IE0031';
            case 'POLI':
                return 'IE0041';
            default:
                return '';
        }
    }
    /**
     * 创建订单直接支付
     *
     * @param unknown $out_trade_no    订单号
     * @param unknown $body            商品名称
     * @param unknown $detail          商品详情
     * @param unknown $total_fee       支付金额
     * @param unknown $pay_type        支付类型
     * @param unknown $notify_url      异步通知url
     * @param unknown $return_url      页面跳转
     * @return unknown
     */
    public function setIePay($out_trade_no, $body, $detail, $total_fee, $notify_url, $return_url, $pay_type)
    {
        $iepay_config = $this->getIepayConfig();
        
        // 支付类型
        $pay_type = $this->changePay_type($pay_type);
        // 必填，不能修改
        // 服务器异步通知页面路径
        $notify_url = $notify_url;
        // 需http://格式的完整路径，不能加?id=123这类自定义参数
        
        // 页面跳转同步通知页面路径
        $return_url = $return_url;
        // 需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/
        
        // 商户订单号
        $out_trade_no = $out_trade_no;
        // 商户网站订单系统中唯一订单号，必填
        
        // 订单名称
        $subject = $body;
        // 必填
        
        // 付款金额
        $total_fee = $total_fee;

        //人民币金额
        $rmb_fee = "";
        // 必填
        
        // 订单描述
         $goods_detail = $detail;

        // 构造要请求的参数数组，无需改动
        $parameter = array(
            "mid" => $iepay_config['mid'],
            "total_fee" => $total_fee*100,
            "rmb_fee" => $rmb_fee,
            "goods" => $body,
            "goods_detail" => $detail,
            "out_trade_no" => $out_trade_no,
            "pay_type" => $pay_type,
            "expired" =>  3600,
            "notify_url" => $notify_url,
            "return_url" => $return_url,
            "version" => "v1",
            "pay_rate" => '',
            "_input_charset" => trim(strtolower($iepay_config['input_charset']))
        );
//        return $parameter;
//        $parameter = array_merge($parArr, $parameter);
        // 建立请求
        $iepaySubmit = new IepaySubmit($iepay_config);
        
        $html_text = $iepaySubmit->buildRequestHttp($parameter, "create");
        // echo $html_text;
        return $html_text;
    }
    /**
     * 查询订单没支付成功继续支付
     *
     * @param unknown $out_trade_no    订单号
     * @param unknown $pay_type        支付类型
     * @return unknown
     */
    public function checkIePay($out_trade_no, $pay_type)
    {
        $iepay_config = $this->getIepayConfig();

        // 支付类型 IE00021
        $pay_type = $this->changePay_type($pay_type);

        // 商户订单号
        $out_trade_no = $out_trade_no;

        // 构造要请求的参数数组，无需改动
        $parameter = array(
            "mid" => $iepay_config['mid'],
            "out_trade_no" => $out_trade_no,
            "pay_type" => $pay_type,
            "version" => "v1",
            "_input_charset" => trim(strtolower($iepay_config['input_charset']))
        );

        // 建立请求
        $iepaySubmit = new IepaySubmit($iepay_config);

        $html_text = $iepaySubmit->buildRequestHttp($parameter, "check");
        // echo $html_text;
        return $html_text;
    }
    /**
     * 退款
     *
     * @param unknown $out_trade_no    订单号
     * @param unknown $total_fee       退款金额
     * @param unknown $pay_type        支付类型
     * @return unknown
     */
    public function refundIePay($out_trade_no, $pay_type,$total_fee)
    {
        $iepay_config = $this->getIepayConfig();

        // 支付类型
        $pay_type = $this->changePay_type($pay_type);

        // 商户订单号
        $out_trade_no = $out_trade_no;
        // 付款金额
        $total_fee = $total_fee;
        // 构造要请求的参数数组，无需改动
        $parameter = array(
            "mid" => $iepay_config['mid'],
            "out_trade_no" => $out_trade_no,
            "refund_amount" => $total_fee*100,
            "pay_type" => $pay_type,
            "version" => "v1",
            "_input_charset" => trim(strtolower($iepay_config['input_charset']))
        );
        // 建立请求
        $iepaySubmit = new IepaySubmit($iepay_config);

        $html_text = $iepaySubmit->buildRequestHttp($parameter, "refund");
        // echo $html_text;
        return $html_text;
    }

}