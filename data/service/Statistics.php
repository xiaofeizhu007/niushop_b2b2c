<?php
/**
 * Shop.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace data\service;

/**
 * 统计服务层
 */
use data\service\BaseService as BaseService;
use data\api\IStatistics;
use think\Cache;
use data\model\NsMemberAccountModel;
use data\model\NsMemberModel;
use data\model\NsMemberAccountRecordsModel;
use data\model\NsMemberAccountRecordsViewModel;
use data\model\UserModel;
use data\model\NsOrderModel;
use data\model\NsOrderGoodsModel;
use data\model\NsShopModel;
use data\model\NsGoodsModel;
use data\model\NsOrderGoodsViewModel;
use data\model\NsAccountReturnRecordsModel;
use data\model\NsAccountOrderRecordsModel;
use data\model\NsAccountRecordsModel;
use data\model\NsAccountWithdrawRecordsModel;
use data\model\NsShopAccountProceedsRecordsModel;
use data\model\NsShopOrderReturnModel;


class Statistics extends BaseService implements IStatistics
{

    function __construct()
    {
        parent::__construct();
    }
	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getMemberCount()
     */
    public function getMemberCount($condition)
    {
        // TODO Auto-generated method stub
        $member_model = new NsMemberModel();
        $member_count = $member_model ->getCount($condition);
        return $member_count;
        
    }

	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getIntegralSum()
     */
    public function getIntegralSum($condition)
    {
        // TODO Auto-generated method stub
        $member_account_model = new NsMemberAccountModel();
        $integral_sum = $member_account_model -> getSum($condition, "member_sum_point");
        return $integral_sum;
    }

	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getGrantIntegralSum()
     */
    public function getGrantIntegralSum($condition)
    {
        // TODO Auto-generated method stub
        $condition["account_type"] = 1;
        $member_account_records_model = new NsMemberAccountRecordsModel();
        $grant_integral_sum = $member_account_records_model -> getSum($condition, "number");
        return $grant_integral_sum;
    }
    public function getGrantBalanceSum($condition)
    {
        // TODO Auto-generated method stub
        $condition["account_type"] = 2;
        $member_account_records_model = new NsMemberAccountRecordsModel();
        $grant_integral_sum = $member_account_records_model -> getSum($condition, "number");
        return $grant_integral_sum;
    }
	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getMemberUseMoney()
     */
    public function getMemberUseMoney($condition, $order_condition)
    {
        // TODO Auto-generated method stub
        $user_model = new UserModel();
        $user_array = $user_model -> getQuery($condition, "uid", '');
        $user_money = 0;
        if(!empty($user_array)){
            $order_model= new NsOrderModel();
            $order_condition = array();
            
            $order_condition["buyer_id"] = ["in", $user_array];
            $user_money = $order_model -> getSum($order_condition, "pay_money");
        }
        return $user_money;
    }

	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getMemberUseNum()
     */
    public function getMemberUseNum($condition,$order_condition)
    {
        // TODO Auto-generated method stub
        $user_model = new UserModel();
        $user_array = $user_model -> getQuery($condition, "uid", '');
        $user_user_num = 0;
        if(!empty($user_array)){
            $order_model= new NsOrderModel();
//             $order_condition = array();
            $order_condition["buyer_id"] = ["in", $user_array];
            $order_condition["pay_status"] = ["gt", 0];
            $user_user_num = $order_model -> getCount($order_condition);
        }
        return $user_user_num;
    }
	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getOrderCount()
     */
    public function getOrderCount($condition)
    {
        // TODO Auto-generated method stub
        $order_model = new NsOrderModel();
        $order_count = $order_model -> getCount($condition);
        return $order_count;
    }

	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getOrderMoney()
     */
    public function getOrderMoney($condition)
    {
        // TODO Auto-generated method stub
        $order_model = new NsOrderModel();
        $order_count = $order_model -> getSum($condition, "pay_money");
        return $order_count;
    }

	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getSaleGoodsCount()
     */
    public function getSaleGoodsCount($condition)
    {
        // TODO Auto-generated method stub
        $order_goods_model = new NsOrderGoodsViewModel();
        $order_goods_num = $order_goods_model ->getOrderGoodsSum($condition);
        return $order_goods_num;
    }

	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getShopCount()
     */
    public function getShopCount($condition)
    {
        // TODO Auto-generated method stub
        $shop_model = new NsShopModel();
        $shop_num = $shop_model -> getCount($condition);
        return $shop_num;
    }

	/* (non-PHPdoc)
     * @see \data\api\IStatistics::getGoodsCount()
     */
    public function getGoodsCount($condition)
    {
        // TODO Auto-generated method stub
        $goods_model = new NsGoodsModel();
        $goods_num =  $goods_model->getCount($condition);
        return $goods_num;
    }
    
    public function getOrderMemberCount($condition){
        $order_model = new NsOrderModel();
        $member_count = $order_model->distinct(true)->field('buyer_id')->where($condition)->count();
        return $member_count;
    }
    
    /**
     * 获取会员注册记录列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     */
    public function getMemberRegRecordsList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '')
    {
    	$user = new UserModel();
    	$list = $user->pageQuery($page_index, $page_size, $condition, $order, $field);
    	return $list;
    }
    /**
     * 获取会员余额记录列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     */
    public function getMemberBalanceRecordsList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '')
    {
    	$account_records = new NsMemberAccountRecordsModel();
    	$list = $account_records->pageQuery($page_index, $page_size, $condition, $order, $field);
    	foreach($list['data'] as $v){
    		$v['user_name'] = $this->getUserName($v['uid']);
    		$v['from_type_name'] = $this->memberAccountRecordsFromType($v['from_type']);
    		$v['shop_name']	= $this->getShopName($v['shop_id']); 
    	}
    	return $list;
    }
    /**
     * 获取会员用户名
     * @param unknown $uid
     */
    public function getUserName($uid)
    {
    	$user = new UserModel();
    	$user_info = $user->getInfo(['uid'=>$uid],'user_name');
    	return $user_info['user_name'];
    	
    }
    public function getShopName($shop_id){
    	$shop = new Shop();	
    	$shop_info = $shop->getShopInfo($shop_id,'shop_name');
    	return $shop_info['shop_name'];
    }
    /**
     * 获取会员流水账产产生方式
     * @param unknown $type
     */
    public function memberAccountRecordsFromType($type)
    {
    	$array = [
    			'1'	=>	'商城订单',
    			'2'	=>	'订单退还',
    			'3'	=>	'兑换',
    			'4'	=>	'充值',
    			'5'	=>	'签到',
    			'6'	=>	'分享',
    			'7'	=>	'注册',
    			'8'	=>	'提现',
    			'9'	=>	'提现退还',
    			'10'=> 	'其他',
    	];
    	return $array[$type];
    }
    
    /**
     * 获取平台利率明细
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     * @return multitype:number unknown
     */
    public function getPlatformAccountRecords($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '')
    {
    	$account_records = new NsAccountReturnRecordsModel();
    	$list = $account_records->pageQuery($page_index, $page_size, $condition, $order, $field);
    	foreach($list['data'] as $v){
    		$v['shop_name']	= $this->getShopName($v['shop_id']);
    	}
    	return $list;
    }
    
    /*
     * 获取平台订单总额对应明细
     */
    public function getPlatformAccountOrderRecordList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '')
    {
        $account_order_record_model = new NsAccountOrderRecordsModel();
        $list = $account_order_record_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        foreach($list['data'] as $v){
            $v['shop_name']	= $this->getShopName($v['shop_id']);
        }
        return $list;
    }
    
    /*
     * 获取营业总额对应明细
     */
    public function getPlatformAccountTotalRecordList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '')
    {
        $account_record_model = new NsAccountRecordsModel();
        $list = $account_record_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        foreach($list['data'] as $v){
            $v['shop_name']	= $this->getShopName($v['shop_id']);
        }
        return $list;
    }

    
    /**
     * 获取店铺提现明细
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     * @return multitype:number unknown
     */
    public function getShopAccountWithdrawList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '')
    {
    	$account_records = new NsAccountWithdrawRecordsModel();
    	$list = $account_records->pageQuery($page_index, $page_size, $condition, $order, $field);
    	foreach($list['data'] as $v){
    		$v['shop_name']	= $this->getShopName($v['shop_id']);
    	}
    	return $list;
    }
    
    /**
     * 获取店铺余额明细
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     * @return multitype:number unknown
     */
    public function getshopAccountProceedsList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '')
    {
    	$account_records = new NsShopAccountProceedsRecordsModel();
    	$list = $account_records->pageQuery($page_index, $page_size, $condition, $order, $field);
    	foreach($list['data'] as $v){
    		$v['shop_name']	= $this->getShopName($v['shop_id']);
    	}
    	return $list;
    }
    
    /**
     * 获取平台销售状况记录
     * @param unknown $page_index
     * @param unknown $page_size
     * @param unknown $condition
     * @param unknown $order
     */
    public function getOrderReturnRecordList($page_index, $page_size, $condition, $order){
        $order_return_model = new NsShopOrderReturnModel();
        $list = $order_return_model->getViewList($page_index, $page_size, $condition, $order);
        return $list;
    }
    
}
