﻿//弹出图片管理界面  2016年11月24日 17:58:41
var OpenPricureDialog = function(type, ADMIN_MAIN, number ,upload_type, spec_id, spec_value_id) {
	//uoload_type 上传来源 1 为商品主图  2 位商品sku图片 3 位规格spec
	if (number  == null || number  == '' ) {
		number  = 0;
	}
	if (type == "PopPicure") {
		art.dialog.open(__URL(ADMIN_MAIN + '/system/dialogalbumlist?number='+ number +'&spec_id='+spec_id+'&spec_value_id='+spec_value_id+'&upload_type='+upload_type), {
			lock : true,
			title : "我的图片",
			width : 900,
			height : 620,
			drag : false,
			background : "#000000"
		}, true);
	}
}
//弹出sku管理界面
var OpenSkuDialog = function(ADMIN_MAIN,attr_id) {
	art.dialog.open(__URL(ADMIN_MAIN + '/goods/controldialogsku?attr_id='+attr_id), {
		lock : true,
		title : "商品规格",
		width : 860,
		height : 350,
		drag : false,
		background : "#000000"
	}, true);
	
}

//弹出商品分类选择界面
var OpenCategoryDialog = function(ADMIN_MAIN, category_id, goodsid, flag, box_id, category_extend_id) {
	var dialog_title = "";
	switch(flag){
	case "category":
		dialog_title = "商品类目";
		break;
	case "extend_category":
		dialog_title = "商品扩展类目";
		break;
	
	}
	art.dialog.open(__URL(ADMIN_MAIN + '/goods/dialogselectcategory?category_id='+category_id+'&goodsid='+goodsid+"&flag="+flag+"&box_id="+box_id+"&category_extend_id="+category_extend_id), {
		lock : true,
		title : dialog_title,
		width : 846,
		height : 516,
		drag : false,
		background : "#000000"
	}, true);
	
	$('iframe').prop('scrolling','no');
	
}

//弹出商品分类选择界面
//type 决定商品是单选还是多选 1：多选 2：单选
//data 商品显示列表的控制数据
var OpenGoodsSelectDialog = function(ADMIN_MAIN,type,obj) {
	var dialog_title = "商品列表";
	var data = JSON.stringify(obj);
	
	art.dialog.open(__URL(ADMIN_MAIN + '/promotion/goodsSelectList?type=' + type + '&data=' + data), {
		lock : true,
		title : dialog_title,
		width : '50%',
		height : 530,
		drag : true,
		background : "grba(0,0,0,0)"
	}, true);
	
}

//弹出会员注册界面
var OpenMemberRegisterDialog = function(PLATFORM_MAIN) {
	var dialog_title = "会员注册";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/memberregrecord'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
	
}
//弹出余额记录界面
var OpenMemberAccountRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "余额记录";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/memberregrecord'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
}
var OpenMemberAccountBalanceRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "余额记录";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/memberBalancelist'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
}
var OpenMemberAccountIntegralRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "积分记录";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/memberPointList'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
}

var OpenPlatformAccountTotalRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "平台总营业额";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/platformAccountTotalRecordList'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
} 

var OpenPlatformAccountBalanceRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "平台资金总余额";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/platformAccountTotalRecordList?issue=0'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
} 

var OpenPlatformAccountOrderRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "商城订单总额";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/platformAccountOrderRecordList'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
} 

var OpenPlatformAccountReturnRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "平台利润总额";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/accountReturnRecordsList'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
} 

var OpenPlatformAccountProceedsRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "店铺总余额";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/shopAccountProceedsList'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
} 

var OpenPlatformAccountWithdrawRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "店铺总提现";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/shopAccountWithdrawList'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
} 

var OpenPlatformAccountPointRecordDialog = function(PLATFORM_MAIN) {
	var dialog_title = "店铺总积分";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/platformAccountPointRecordList'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
var OpenPlatformOrderAccountDialog =  function(PLATFORM_MAIN) {
	var dialog_title = "订单总计";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/platformAccountOrderRecordList'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 800,
		drag : false,
		background : "#000000"
	}, true);
}
} 

var OpenPlatformWaitProfitRecordDialog = function(PLATFORM_MAIN){
	var dialog_title = "平台待结算利润";
	art.dialog.open(__URL(PLATFORM_MAIN + '/statistics/platformWaitProfitList'), {
		lock : true,
		title : dialog_title,
		width : 1050,
		height : 600,
		drag : false,
		background : "#000000"
	}, true);
}