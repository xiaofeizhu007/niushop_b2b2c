/**
 * 地理位置处理类
 */
/**
 * 设置定位城市
 */
function setCityLocation(city, is_refresh){
	is_refresh = typeof is_refresh !== 'undefined' ?  is_refresh : true;
	$.ajax({
		type : "post",
		url: __URL(APPMAIN+'/Geographical/setCitySesson'),
		data : {"city" : city},
		success: function (res) {
			if(is_refresh){
				location.reload();
			}
		}
	})
}
//定位
function getLocation(callback){
	AMap.plugin('AMap.Geolocation', function() {
	    geolocation = new AMap.Geolocation({
	        enableHighAccuracy: true,//是否使用高精度定位，默认:true
	        timeout: 10000,          //超过10秒后停止定位，默认：无穷大
	    });
	    geolocation.getCurrentPosition();
	    AMap.event.addListener(geolocation, 'complete', function(res){callback(res);});//返回定位信息
	    AMap.event.addListener(geolocation, 'error',function(){callback('');});      //返回定位出错信息
	})
 	
}