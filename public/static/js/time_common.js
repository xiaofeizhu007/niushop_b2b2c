/**
 * 事件处理类
 */
//时间戳转时间类型
function timeStampTurnTime(timeStamp){
	if(timeStamp > 0){
		var date = new Date();  
		date.setTime(timeStamp * 1000);
		var y = date.getFullYear();      
		var m = date.getMonth() + 1;      
		m = m < 10 ? ('0' + m) : m;      
		var d = date.getDate();      
		d = d < 10 ? ('0' + d) : d;      
		var h = date.getHours();    
		h = h < 10 ? ('0' + h) : h;    
		var minute = date.getMinutes();    
		var second = date.getSeconds();    
		minute = minute < 10 ? ('0' + minute) : minute;      
		second = second < 10 ? ('0' + second) : second;     
		return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second;  		
	}else{
		return "";
	}
	    
	//return new Date(parseInt(time_stamp) * 1000).toLocaleString().replace(/年|月/g, "/").replace(/日/g, " ");
}

//是否处于夏令时
function isInDayLightTime()
{
    var date = new Date();
    var currentTimezoneOffset = date.getTimezoneOffset();

    //设置时间为年初
    date.setMonth(0);
    date.setDate(1);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);

    var startDateTimezoneOffset = date.getTimezoneOffset();

    //设置时间为年中
    date.setMonth(6);

    var middleDateTimezoneOffset = date.getTimezoneOffset();

    var isDst = false;
    if(startDateTimezoneOffset !== middleDateTimezoneOffset){
        if(currentTimezoneOffset >= startDateTimezoneOffset && currentTimezoneOffset >= middleDateTimezoneOffset){
            isDst = true;
        }
    }
    return isDst;
}

function getClientTimezone()
{
    var tz = jstz.determine();
    var clientTimezone = tz.name();
    var isDst = isInDayLightTime();
    $.ajax({
        url : __URL(APPMAIN+"/Timezone/getClientTimezone"),
        type : "post",
        dataType : "json",
		data : {
        	'clientTimezone' : clientTimezone,
			'isDst' : isDst
		},
        success : function(res) {
        	console.log(res);
        	//window.location.reload();
        }
    });
}





