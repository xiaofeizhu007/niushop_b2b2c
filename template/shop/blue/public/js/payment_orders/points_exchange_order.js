/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : 王永杰
 * @date : 2017年6月14日 12:01:47
 * @version : v1.0.0.0
 * PC端待付款订单
 * 更新时间：2017年6月22日 14:19:56
 */
$(function(){
	//初始化函数
	init();
	/**
	 * 选择物流公司,计算运费
	 */
	$(".express_company").change(function(){
		var shop_id = $(this).attr("shop-id");
		$(".shop-goods-box[shop-id="+ shop_id +"] .express_money").val($(this).children("option:selected").attr("data-express-fee"));
	});
	/**
	 * 提交订单
	 */
	var flag = false;//防止重复提交
	$(".btn-jiesuan").click(function(){
		if(validationOrder()){
			if(flag){
				return;
			}
			flag = true;
			var pick_up_id_str = "";
			var express_company_str = "";
			var user_coupon_str = ""; //优惠券
			var shop_reamrk_str = ""; //留言
			
			
			$(".shop-goods-box").each(function() {
				var shop_id = $(this).attr("shop-id");
				//拼装店铺物流公司 
				if(express_company_str == ""){
					express_company_str = shop_id + "," + $(this).find(".express_company").val();
				}else{
					express_company_str += ";" + shop_id + "," + $(this).find(".express_company").val();
				}
				//拼装店铺自提点
				if(pick_up_id_str == ''){
					pick_up_id_str = shop_id + "," +getPickupId(shop_id);
				}else{
					pick_up_id_str += ";" + shop_id + "," +getPickupId(shop_id);
				}
				//拼装留言
				var shop_reamrk = $(this).find(".shop_reamrk").val();
				if(shop_reamrk_str == ""){
					shop_reamrk_str = shop_id + "," + shop_reamrk;
				}else{
					shop_reamrk_str += ";" + shop_id + "," + shop_reamrk;
				}
			});			
			var goods_sku_list = $("#goods_sku_list").val();// 商品Skulist
			//var leavemessage = $("#leavemessage").val();// 订单留言
			var account_balance = 0;//可用余额
			var integral = $("#hidden_count_points").val();//积分;
			var pay_type = 0;//支付方式 0：在线支付，4：货到付款
			var buyer_invoice = ''; //发票
			$.ajax({
				url : __URL(SHOPMAIN + "/order/exchangeOrderCreate"),
				type : "post",
				data : {
					'goods_sku_list' : goods_sku_list, 
					'leavemessage' : shop_reamrk_str,  //留言
					'use_coupon' : user_coupon_str,
					'integral' : integral,
					'account_balance' : account_balance,
					'pay_type' : pay_type,
					'buyer_invoice' : buyer_invoice,
					'pick_up_id' : pick_up_id_str, //拼装店铺自提点
					'express_company_id' : express_company_str //物流
				},
				success : function(res) {
					if (res.code > 0) {
						$(".btn-jiesuan").css("background-color","#ccc");
						//如果不等于4 实际付款金额为0，跳转到个人中心的订单界面中
						if(pay_type == 4){
							location.href = __URL(SHOPMAIN + '/member/orderlist');
						}else{
							location.href = __URL(APPMAIN + '/pay/paycallback?msg=1&out_trade_no=' + res.code);
						}
					}else{
						$.msg(res.message);
						flag = false;
					}
				}
			});
		}
	});
	
	/**
	 * 选择配送方式
	 * 2017年6月20日 10:53:18 王永杰
	 */
	$(".shipping_method .js-select li").click(function(){
		var shop_id = $(this).attr("shop-id");
		if($(this).attr("data-code") == "no_config"){
			return;
		}
		$(".shop-goods-box[shop-id="+ shop_id +"] .shipping_method .js-select li i").hide();
		$(".shop-goods-box[shop-id="+ shop_id +"] .shipping_method .js-select li a").removeClass("selected");
		$(this).children("i").show();
		$(this).children("a").addClass("selected");
		switch($(this).attr("data-code")){
			case "merchant_distribution":
				//商家配送
				$(".shop-goods-box[shop-id="+ shop_id +"] .js-pickup-point-list").hide();//隐藏自提列表
				//如果后台开启了选择物流公司，则显示
				if(parseInt($("#hidden_is_logistice").val()) === 1) $(".shop-goods-box[shop-id="+ shop_id +"] .select_express_company").slideDown(300);
				else $(".shop-goods-box[shop-id="+ shop_id +"] .select_express_company").slideUp(300);//物流公司显示
			break;
			case "afhalen":
				//门店自提
				$(".shop-goods-box[shop-id="+ shop_id +"] .js-pickup-point-list").show();//显示自提列表
				$(".shop-goods-box[shop-id="+ shop_id +"] .select_express_company").hide();//隐藏物流公司
			break;
		}
	})
	

	
	
});


/**
 * 验证
 * @param is_show
 * @returns {Boolean}
 */
function validationOrder(){
	var total_point = $("#hidden_count_points").val();//商品总积分
	var my_point = $("#hidden_point").val();//我的账户积分
	if (parseFloat(total_point) > parseFloat(my_point)) {
		$.msg("当前积分不足");
		return false;
	}
	
	if ($("#address_id").val() == undefined ||$("#address_id").val() == '' || $("#address_id").val() == 0) {
		$.msg("请先选择收货地址");
		return false;
	}
	
	var shop_error = true;
	var shop_error_message = "";
	$(".shop-goods-box").each(function() {
		
		if($(this).find(".shipping_method li[data-code='no_config']").attr("data-code")){
			$.msg("商家未配置配送方式");
			shop_error_message = "商家未配置配送方式";
			shop_error = false;
			return false
		}
		
		var is_selected_shipping = 0;
		$(this).find(".shipping_method .js-select li a").each(function(){
			if($(this).hasClass("selected")){
				is_selected_shipping ++;
			}
		});
		//防止手动取消配送方式
		if(!is_selected_shipping){
			shop_error_message = "商家未配置配送方式";
			shop_error = false;
			return false
		}

		var merchant_distribution = $("#shipping_method .js-select li[data-code='merchant_distribution']");//商家配送
		if(merchant_distribution.children("a").hasClass("selected")){
			if(parseInt($(this).find(".express_company").val()) == -1){
				shop_error_message = "商家未设置物流公司";
				shop_error = false;
				return false
			}else if(parseInt($(this).find(".express_company").val()) == -2){
				shop_error_message = "商家未配置物流公司运费模板";
				shop_error = false;
				return false
			}else if(parseInt($(this).find(".express_company").val()) == 0){
				shop_error_message = "未知错误";
				shop_error = false;
				return false
			}
		}
		//选择门店自提
		if($(this).find(".shipping_method li[data-code='afhalen'] a").hasClass("selected")){
			//如果没有自提列表，后台需要编辑
			if($(this).find(".pickup_address").val() == undefined){
				shop_error_message = "商家未配置自提点，请选择其他配送方式";
				shop_error = false;
				return false
			}
		}
		
	});
	if(!shop_error){
		$.msg(shop_error_message);
		return false
	}
	
	return true;
}



/**
 * 获取自提地址id
 */
function getPickupId(shop_id){
	var id = 0;
	if($(".shop-goods-box[shop-id="+ shop_id +"] .shipping_method li[data-code='afhalen'] a").hasClass("selected")){
		id = parseInt($("#pickup_address").val());
	}
	return id;
}



/**
 * 初始化 计算各费用
 */
function init(){
	$(".shop-goods-box").each(function() {
		var shop_event = $(this);
		/**
		 * 选中第一个配送方式对应的更新数据
		 */
		if(shop_event.find(".shipping_method .js-select li").length){
			shop_event.find(".shipping_method .js-select li").each(function(i){
				if(i==0){
					switch($(this).attr("data-code")){
					case 'merchant_distribution':
						//商家配送
						//如果后台开启了选择物流公司，则显示
						if(parseInt($("#hidden_is_logistice").val()) === 1) {
							shop_event.find(".select_express_company").slideDown(300);
							shop_event.find(".express_money").val(shop_event.find(".express_company option:selected").attr("data-express-fee"));
						}else{
							shop_event.find(".select_express_company").slideUp(300);//物流公司显示
						}
						break;
					case 'afhalen':
						shop_event.find(".js-pickup-point-list").slideDown(300);//自提列表显示
						break;
					}
					$(this).children("i").show();
					$(this).children("a").addClass("selected");
				}
				return false;
			});
		}else{
			shop_event.find(".shipping_method .js-select").html('<p class="label fl">商家未配置配送方式</p>');
		}
	});

}



