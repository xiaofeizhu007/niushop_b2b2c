/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : 王永杰
 * @date : 2017年6月14日 12:01:47
 * @version : v1.0.0.0
 * 手机端待付款订单
 * 更新时间：2017年6月22日 14:19:56
 * 
 * 实时更新的应付金额，受以下几项影响：
 * 1、优惠券      use-coupon
 * 2、支付方式     pay
 * 3、配送方式     distribution
 * 4、使用余额
 * 5、需要发票     invoice
 * 6、选择物流公司
 * 
 */
$(function() {
	
	//初始化数据
	init();

	/**
	 * 弹出框，该方法只负责界面展示，不负责计算
	 * 选择优惠券 use-coupon
	 * 选择支付方式 pay
	 * 选择配送方式 distribution
	 * 选择自提地址列表 pickup_address
	 * 选择发票信息 invoice
	 * 选择发票内容 invoice-content
	 * 
	 * 2017年6月21日 14:16:57 王永杰
	 */
	$(".item-options[data-flag]").click(function() {
		var curr_options = $(this);//当前点击的项
		var flag = curr_options.attr('data-flag');
		var flag_shop = curr_options.attr('data-flag-shop');
		var shop_id = curr_options.attr("shop-id");
		if(flag != undefined){
			$(".mask-layer").fadeIn(300);
			$(".mask-layer-control[data-flag='" + flag_shop + "']").slideDown(300);
			if(getCurrMaskLayer() != null){
				getCurrMaskLayer().find("li").click(function(){
					var curr_li = $(this);
					getCurrMaskLayer().find("li").removeClass("active");
					curr_li.addClass("active");
					var msg = curr_li.children("div:last").text();//内容
					switch(flag){
					
						case "distribution":
							//当前打开的是配送方式
							updateDistribution(curr_li, shop_id);
							break;
							
						case "express_company":
							//当前打开的是物流公司
							updateExpressCompany(curr_li, shop_id);
							break;
							
						case "pickup_address":
							//当前打开的是自提地址列表
							updatePickupAddress(curr_li, shop_id);
							break;
						case "shipping_time":
							//当前打开的是指定配送时间
							updateShippingTime(curr_li, shop_id);
							break;
					}
					curr_options.children("span").text(msg);
					getCurrMaskLayer().slideUp(300);
					$(".mask-layer").fadeOut(300);
					
				});
			}
		}
	});
	

	/**
	 * 关闭弹出框（包括点击遮罩层、确定按钮、右上角X按钮）
	 * 2017年6月21日 14:18:15 王永杰
	 */
	$(".mask-layer,.btn-green,.mask-layer-control .close").click(function() {
		getCurrMaskLayer().slideUp(300);
		$(".mask-layer").fadeOut(300);
	});

	/**
	 * 删除已选择的指定配送时间
	 */
	$(".shipping_time .delete").click(function(){
		var shop_id = $(this).attr("shop-id");
		var default_time = $(".shop-box[shop-id='"+shop_id+"']").find(".shipping_time .time").attr('data-default');
		$(".shop-box[shop-id='"+shop_id+"']").find(".shipping_time .time").html(default_time);
		$(".shop-box[shop-id='"+shop_id+"']").find(".hidden_shipping_time").val(0);
		$(".shop-box[shop-id='"+shop_id+"']").find(".shipping_time .delete").hide();
		$(".shop-box[shop-id='"+shop_id+"']").find(".date-list p").removeClass('on');
		return false;
	});
});


/**
 * 初始化数据，仅在第一次加载时使用
 * 2017年6月22日 14:59:33 王永杰
 */
function init(){

	/**
	 * 选中第一个配送方式对应的更新数据
	 * 2017年6月28日 17:33:19
	 */
	$(".shop-box").each(function(t){
		var shop_event = $(this);
		
		var shop_id = shop_event.attr("shop-id");
		loading_shipping_time(shop_id);//拼装物流时间
		//物流方式计算运费
		if(shop_event.find(".mask-layer-control[data-attr-shop-name='distribution'] li").length){
			shop_event.find(".mask-layer-control[data-attr-shop-name='distribution'] li").each(function(i){
				if(i==0){
					switch(parseInt($(this).attr("data-flag"))){
					case 1:
						//商家配送
						//如果后台开启了选择物流则显示

						if(parseInt(shop_event.find(".mask-layer-control[data-flag='distribution-"+ shop_id +"']").attr("data-is-logistics")) === 1){
							$(".item-options[data-flag-shop='express_company-"+ shop_id +"']").slideDown(300);//物流公司显示
							
							//选中1默认所选的物流公司的价格及id
							var curr_li = shop_event.find(".mask-layer-control[data-flag='express_company-"+ shop_id +"'] li.active");
							//判断是否有物流公司
							if(curr_li.length > 0){
								var co_id = curr_li.attr("data-coid");//物流公司
								var express_fee = parseFloat(curr_li.attr("data-express-fee")).toFixed(2);//运费
								shop_event.find(".item-options[data-flag='express_company']").attr("data-select",co_id);
								shop_event.find(".item-options[data-flag='express_company']").attr("data-express-fee",express_fee);
							}else{
								var express_fee = parseFloat(0).toFixed(2);
							}
							//如果默认选中物流,物流运费是默认值
							shop_event.find(".express_money").val(express_fee);
						}else{  
							shop_event.find(".item-options[data-flag-shop='express_company-"+ shop_id +"']").slideUp(300);//物流公司隐藏
						}
						shop_event.find(".item-options[data-flag-shop='shipping_time-"+ shop_id +"']").slideDown(300);
						break;
					case 2:
						
						//门店自提
						var pickup_address = shop_event.find(".item-options[data-flag-shop='pickup_address-"+ shop_id +"']");//自提地址
						var pickup_count = parseInt(pickup_address.attr("data-count"));//自提列表数量
						var address = "商家未配置自提点";
						if(pickup_count){
							var active_li = shop_event.find(".mask-layer-control[data-flag='pickup_address-"+ shop_id +"'] li.active");
							address = active_li.children("div:last").text();
							pickup_address.attr("data-id",active_li.attr("data-id"));
						}else{
							pickup_address.children("span").removeClass("arrow-right");
						}
						pickup_address.children("span").html(address);
						shop_event.find(".item-options[data-flag-shop='distribution-"+ shop_id +"']").attr("data-select",1);//只有是门店自提，就免邮
						pickup_address.slideDown(300);//自提列表显示
						
						break;
					case 3:
						//本地配送
						shop_event.find(".item-options[data-flag='distribution']").attr("data-select",2);
						break;
					}
					$(this).addClass("active");
				}
				return false;
			});
		}else{
			//没有配送方式
			$(".item-options[data-flag-shop='distribution-"+shop_id+"']").attr("data-select",-1);//只有是门店自提，就免邮
		}
	})
}


/**
 * 获取当前打开的弹出框对象
 * 2017年6月21日 14:19:20 王永杰
 */
function getCurrMaskLayer(){
	return $(".mask-layer-control:visible");
}
/**
 * 更新配送方式，如果选择的是门店自提则显示自提地址
 * 2017年6月21日 17:11:05 王永杰
 * 
 * @param curr_li 当前选中的配送方式
 */
/**
 * 更新配送方式，如果选择的是门店自提则显示自提地址
 * @param curr_li 当前选中的配送方式
 */
function updateDistribution(curr_li, shop_id){
	var flag = parseInt(curr_li.attr("data-flag"));//1：配送方式，2：门店自提
	var pickup_address = $(".item-options[data-flag-shop='pickup_address-"+ shop_id +"']");//自提地址选项
	var pickup_address_span = pickup_address.children("span");//自提地址内容
	var express_company = $(".item-options[data-flag-shop='express_company-"+ shop_id +"']");//物流公司（配送方式显示，门店自提隐藏）
	var pickup_address_mask = $(".mask-layer-control[data-flag='pickup_address-"+ shop_id +"']");//自提地址弹出框
	var active_li = pickup_address_mask.find("li.active");
	switch(flag){
	case 1:
		$(".item-options[data-flag-shop='distribution-"+ shop_id +"']").attr("data-select",0);
		pickup_address.slideUp(300);//隐藏自提地址
		//如果后台开启了选择物流则显示
		if(parseInt($(".mask-layer-control[data-flag='distribution-"+ shop_id +"']").attr("data-is-logistics")) === 1) express_company.slideDown(300);//物流公司显示
		else express_company.slideUp(300);//物流公司隐藏

		$(".shop-box[shop-id='"+shop_id+"").find(".distribution_time").slideUp(300);//本地配送时间隐藏
		$(".shop-box[shop-id='"+shop_id+"").find(".item-options[data-flag-shop='shipping_time-"+ shop_id +"']").slideDown(300);
		break;
	case 2:
		express_company.slideUp(300);
		$(".item-options[data-flag-shop='distribution-"+ shop_id +"']").attr("data-select",1);
		var address = active_li.children("div:last").text();
		pickup_address.attr("data-id",active_li.attr("data-id"));
		//处理显示方式
		if(pickup_address.attr("data-count")> 0){
			if(address.length>20){
				pickup_address_span.removeClass("arrow-right");
			}
			pickup_address_span.addClass("arrow-right");
		}else{
			address = pickup_address_mask.find("p").text();
			pickup_address_span.removeClass("arrow-right");
		}
		pickup_address_span.html(address);//设置选择后的自提地址
		pickup_address.slideDown(300);//显示自提地址

		$(".shop-box[shop-id='"+shop_id+"").find(".distribution_time").slideUp(300);//本地配送时间隐藏
		$(".item-options[data-flag-shop='shipping_time-"+ shop_id +"']").slideUp(300);//隐藏配送时间
		break;
	}
}

/**
 * 选择物流公司，更新运费
 * 2017年6月28日 16:21:47 王永杰
 * @param curr_li 当前选中的物流公司
 */
function updateExpressCompany(curr_li, shop_id){
	
	var co_id = curr_li.attr("data-coid");//物流公司
	var express_fee = parseFloat(curr_li.attr("data-express-fee")).toFixed(2);//运费
	$(".item-options[data-flag-shop='express_company-"+ shop_id +"']").attr("data-select",co_id);
	$(".item-options[data-flag-shop='express_company-"+ shop_id +"']").attr("data-express-fee",express_fee);
	$(".shop-box").find(".express_money").val(express_fee);
}


/**
 * 更新自提地址
 * 2017年6月21日 17:14:32 王永杰
 * @param curr_li
 */
function updatePickupAddress(curr_li, shop_id){
	var msg = curr_li.children("div:last").text();//内容
	var pickup_address = $(".item-options[data-flag-shop='pickup_address-"+ shop_id +"']");//自提地址选项
	if(msg.length>20){
		pickup_address.children("span").removeClass("arrow-right");
	}else{
		pickup_address.children("span").addClass("arrow-right");
	}
	var pickup_address_mask = $(".mask-layer-control[data-flag='pickup_address-"+ shop_id +"']");//自提地址弹出框
	var active_li = pickup_address_mask.find("li.active");
	pickup_address.attr("data-id",active_li.attr("data-id"));
}

/**
 * 获取自提地址id
 * 2017年6月20日 17:13:25 王永杰
 */
function getPickupId(shop_id){
	var id = 0;
	if(parseInt($(".item-options[data-flag-shop='distribution-"+ shop_id +"'][data-flag='distribution']").attr("data-select")) == 1){
		id = parseInt($(".item-options[data-flag-shop='pickup_address-"+ shop_id +"'][data-flag='pickup_address']").attr("data-id"));
	}
	return id;
}

/**
 * 验证订单数据
 * 2017年6月22日 15:08:10 王永杰
 * 
 * @returns true:验证成功，false：验证失败
 */
function validationOrder(){
	
	
	var total_point = $("#hidden_count_points").val();//商品总积分
	var my_point = $("#hidden_point").val();//我的账户积分

	if (parseFloat(total_point) > parseFloat(my_point)) {
		// $.msg("当前积分不足");
        showBox("当前积分不足");
		return false;
	}	
	
	if ($("#addressid").val() == undefined ||$("#addressid").val() == '' ) {
		showBox("请先选择收货地址");
		return false;
	}
	/**
	 * 循环检查商家的物流设置
	 */
	var shop_error = true;
	var shop_error_message = "";
	$(".shop-box").each(function(i){
		shop_event = $(this);
		var shop_id = shop_event.attr("shop-id");
		
		if(parseInt(shop_event.find(".item-options[data-flag='distribution']").attr("data-select")) == -1){
			shop_error = false;
			shop_error_message = "商家未配置配送方式";
			return false;
		}
		if(parseInt(shop_event.find(".item-options[data-flag='distribution']").attr("data-select")) == 0){
			var express_company_flag = false;
			var express_company_msg = "";
			var express_company_select = parseInt(shop_event.find(".item-options[data-flag='express_company']").attr("data-select"));
			if(express_company_select == undefined || express_company_select == ""){
				express_company_select = 0;
			}
			
			switch(express_company_select){
				case -1:
					express_company_flag = true;
					shop_error_message ="商家未设置物流公司";
					break;
				case -2:
					express_company_flag = true;
					shop_error_message ="商家未配置物流公司运费模板";
					break;
				case 0:
					express_company_flag = true;
					shop_error_message = "未知错误";
					break;
			}
			if(express_company_flag){
				shop_error = false;
				return false
			}
		}
		//判断门店自提是否可用
		if(parseInt(shop_event.find(".item-options[data-flag='distribution']").attr("data-select")) == 1){
			//选择门店自提
			if(parseInt(shop_event.find(".item-options[data-flag='pickup_address']").attr("data-count"))==0){
				shop_error_message = "商家未配置自提点，请选择其他配送方式";
				shop_error = false;
				return false;
			}
		}
	})
	if(!shop_error){
		showBox(shop_error_message);
		return false
	}

	return true;
}

/**
 * 提交订单
 * 2017年6月22日 15:09:08 王永杰
 */
var flag = false;//防止重复提交
function submitOrder() {
	if(validationOrder()){
		if(flag){
			return;
		}
		flag = true;
		var goods_sku_list = $("#goods_sku_list").val();// 商品Skulist
		var account_balance = 0;//可用余额
		var pay_type = 11;//支付方式 0：在线支付，4：货到付款
		var buyer_invoice = '';//发票
		var integral = $("#hidden_count_points").val(); //订单总积分

        var express_company = $(".item-options[data-flag='express_company']").attr("data-select")
        var pick_up_id = $(".item-options[data-flag='pickup_address']").attr("data-id");
      //拼装留言
		var shop_reamrk = $(".leavemessage").val();
		$.ajax({
			url : __URL(APPMAIN + "/order/exchangeOrderCreate/"),
			type : "post",
			data : {
				'goods_sku_list' : goods_sku_list,
				'leavemessage' : shop_reamrk,
				'use_coupon' : 0,
				'integral' : integral,
				'account_balance' : account_balance,
				'pay_type' : pay_type,
				'buyer_invoice' : buyer_invoice,
				'pick_up_id' : pick_up_id,
				'shipping_company_id' : express_company
			},
			success : function(res) {
				if (res.code > 0) {
					//如果不是，4 实际付款金额为0，跳转到个人中心的订单界面中
					if(pay_type == 4){
						location.href = __URL(APPMAIN + '/order/myorderlist');
					}else{
						location.href = __URL(APPMAIN + '/pay/paycallback?msg=1&out_trade_no=' + res.code);
					}
				} else {
					showBox(res.message);
					flag = false;
				}
			}
		});
	}
}



/**
 * 更新指定配送时间
 * 创建时间：2018年1月22日12:17:20 全栈小学生
 * @param curr_li
 */
function updateShippingTime(curr_li, shop_id){
	var shipping_time = $(curr_li).attr('data-shipping-time');
	$(".shop-box[shop-id="+shop_id+"]").find(".hidden_shipping_time").val(shipping_time);
	$(".shop-box[shop-id="+shop_id+"]").find(".shipping_time .time").html($(curr_li).html());
	$(".shop-box[shop-id="+shop_id+"]").find(".shipping_time .delete").show();
}

/**
 * 生成未来一个月的配送时间
 * 2018年1月6日 10:20:20 赵海雷
 */
function loading_shipping_time(shop_id){
	
	var week_arr = ["周日","周一","周二","周三","周四","周五","周六"];
	var MIN = 1;//配送时间至少需要两天
	var html = '';
	html += '<ul>';
	for(var i = MIN;i < 30 + MIN;i ++){
		var date = new Date();
		date.setDate(date.getDate() + i);
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		var week = week_arr[date.getDay()];
		var time = Math.round(date.getTime()/1000);
		
		html += '<li data-shipping-time="'+ time +'" >'+ year +'-'+ month +'-'+ day +'&nbsp;'+ week +'</li>';
	}
	html += '</ul>';
	
	$(".shop-box[shop-id="+shop_id+"]").find(".mask-layer-control .date-list").html(html);
}







